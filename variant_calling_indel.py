#!/home/bioinfo/miniconda3/envs/pacbioenv/bin/python

# -*- coding: utf-8 -*-


from multiprocessing import Process, Lock
import first_pacbio_core_V2 as step
import multiprocessing
import argparse
import argparse
import csv
import datetime
import glob
import os
import datetime
import subprocess
import sys
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import seaborn as sns1
matplotlib.use('Agg')
import matplotlib.pyplot as plt1
from matplotlib.ticker import StrMethodFormatter
import scipy.stats as st
import numpy as np
from os import listdir, system
from os.path import isfile, join
import time
pd.options.mode.chained_assignment = None
#import first_diagnosys_core_V2 as step
#from cutCDS import cutCDS,cutCDS37
from scipy.stats import zscore

path ='/home/remo/PROJECT/pacbio/bin/'

def InputPar():
    ####Introducing arguments
	parser = argparse.ArgumentParser(prog= 'MAGI EUREGIO DIAGNOSYS',description='Pipe from BAM to VCF ',
			epilog='Need to SAMTOOLS v1.9 and in global path or put your path [--samtools] [--bcftools]')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
	                    help='[Default = Pipe run in same folder when launch command]')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME',
	                    help='Insert Family name to create Folder')

	# parser.add_argument('-g','--genome', metavar='choose Assembling', choices=['geno37','geno38'],
	#                     default='geno38',
	#                     help='Choices: geno37, geno38 Run Analysis with genome GRCh38 or GRCh37')
	#
	# parser.add_argument('-q','--quality',  metavar='QUALITYFILTER',default=18,
	#                     help=' Choose a quality threshold [Default = 18]')
	#
	# parser.add_argument('-N','--threads', metavar='THREADS', default='8',
	#                     help='Number of threads to run [8]')

	parser.add_argument('-s','--samtools', metavar='SAMTOOLS', default='samtools',
	                    help='Insert SAMTOOLS path')
	parser.add_argument('-sa','--sample', metavar='sample',
	                    help='Insert sample to be analyzed')
	# parser.add_argument('-s','--samtools', metavar='SAMTOOLS', default='samtools',
	#                     help='Insert SAMTOOLS path')
	#
	# parser.add_argument('-v','--bcftools', metavar='BCFTOOLS', default='bcftools',
	#                     help='Insert BCFTOOLS path')
	#
	# parser.add_argument('-t','--samstat', metavar='SAMSTAT', default='samstat',
	#                     help='Insert SAMSTAT path')
	#
	# parser.add_argument('-gk','--gatk', metavar='GATK', default='gatk', help='Insert gatk path')
	#
	# parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
	#                     default='False',
	#                     help='Choices: Attention option False overwrite old data [Default = False] ')
	#
	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z'],
	                    required=True,
	                    help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca,  - required ')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Pannelli Consigliati: 1:CLM2; 2:OSSNEW; 3:OCULARE; 4:OCULARE2; 5:SIFSR; 6:ALLGENE;'
					'7:trusightone; 8:GENETEST1; 9:GENETEST2; 10:CANCER; 11:MALFORMATIONI VASCOLARI;'
					'12:ANOMALIE VASCOLARI; 13:INFERTILITA; 14:OBESITA; 15:GENODERMATOSI\nRequired')

#	parser.add_argument('-o','--name', metavar='Output ', required=True,help='Choose an output filename (Required)')
	return parser.parse_args()

def count_coverage(folder_indels,sample,TO_COUNT):
	TO_COUNT=TO_COUNT[['CHROM','POS','DEPTH','CALL']]

	TO_COUNT['.']=TO_COUNT['CALL'].str.count(r'[.]')
	TO_COUNT[',']=TO_COUNT['CALL'].str.count(r'[,]')
	TO_COUNT['*']=TO_COUNT['CALL'].str.count(r'[*]')
	TO_COUNT['$']=TO_COUNT['CALL'].str.count(r'[$]')
	TO_COUNT['C']=TO_COUNT['CALL'].str.count(r'[cC]')
	TO_COUNT['G']=TO_COUNT['CALL'].str.count(r'[gG]')
	TO_COUNT['T']=TO_COUNT['CALL'].str.count(r'[tT]')
	TO_COUNT['A']=TO_COUNT['CALL'].str.count(r'[aA]')
	TO_COUNT['ins']=TO_COUNT['CALL'].str.count(r'\+[0-9]+[ACGTNacgtn]+')
	TO_COUNT['insA']=TO_COUNT['CALL'].str.count(r'\+[0-9]+[Aa]+')
	TO_COUNT['insC']=TO_COUNT['CALL'].str.count(r'\+[0-9]+[Cc]+')
	TO_COUNT['insG']=TO_COUNT['CALL'].str.count(r'\+[0-9]+[Gg]+')
	TO_COUNT['insT']=TO_COUNT['CALL'].str.count(r'\+[0-9]+[Tt]+')
	TO_COUNT['del']=TO_COUNT['CALL'].str.count(r'\-[0-9]+[ACGTNacgtn]+')
	TO_COUNT['delA']=TO_COUNT['CALL'].str.count(r'\-[0-9]+[Aa]+')
	TO_COUNT['delC']=TO_COUNT['CALL'].str.count(r'\-[0-9]+[Cc]+')
	TO_COUNT['delT']=TO_COUNT['CALL'].str.count(r'\-[0-9]+[Tt]+')
	TO_COUNT['delG']=TO_COUNT['CALL'].str.count(r'\-[0-9]+[Gg]+')
	TO_COUNT['other']=TO_COUNT['CALL'].str.count(r'[*><$^]')

	TO_COUNT['sum'] =TO_COUNT['.']+TO_COUNT[',']+TO_COUNT['.']+TO_COUNT['C']+TO_COUNT['T']+TO_COUNT['A']+TO_COUNT['G']+TO_COUNT['ins']+TO_COUNT['del']

	TO_COUNT['#CHROM'] = TO_COUNT['CHROM']
	TO_COUNT = TO_COUNT[['#CHROM','POS','DEPTH','C','G','T','A','ins', 'insA', 'insC','insT', 'insG','del','delA','delC','delT','delG','sum']]

	TO_COUNT['DEPTH'].fillna(0,inplace=True)
	TO_COUNT['sum'].fillna(0,inplace=True)

	TO_COUNT['sum']=TO_COUNT['sum'].astype(int)
	TO_COUNT['POS']=TO_COUNT['POS'].astype(int)
	TO_COUNT['DEPTH']=TO_COUNT['DEPTH'].astype(int)

	TO_COUNT['selection'] = 1
	TO_COUNT_final=TO_COUNT[['#CHROM','POS','DEPTH','C','G','T','A','ins', 'insA', 'insC','insT', 'insG','del','delA','delC','delT','delG','sum','selection']]
	print ('COUNT OK!!!')
	print ('all count:',len(TO_COUNT_final))

	name_filter_intermedio = (sample+'_unbalance_inter.txt')
	file_filtered_intermedio = join(folder_indels,name_filter_intermedio)

	#print(TO_COUNT_final[TO_COUNT_final['POS']==68438248])
	print(TO_COUNT_final.head(10))

	TO_COUNT_final.to_csv(file_filtered_intermedio,sep='\t',index=False)
	return

def filter_for_panel(folder_indels,name,sample):
	name_filter_intermedio = (sample+'_unbalance_inter.txt')
	# print(folder)
	# print(name)
	# print(name_filter_intermedio)
	file_filtered_intermedio = join(folder_indels,name_filter_intermedio)
	print(file_filtered_intermedio)
	COUNT = pd.read_csv(file_filtered_intermedio,sep='\t',header=0)

	#vertical['filt'] = 1

	name_filter = (sample+'_unbalance_filter.txt')
	file_filtered = join(folder_indels,name_filter)
	print ('-------FILTERING-------')
	#FILTER = pd.merge(COUNT,vertical,on=['#CHROM','POS'],how='left')
	FILTER = COUNT

	FILTER['DEPTH'].fillna(0,inplace=True)
	FILTER['sum'].fillna(0,inplace=True)
	FILTER['selection'].fillna(0,inplace=True)
	#FILTER['filt'].fillna(0,inplace=True)

	#FILTER = FILTER[FILTER['filt']==1]

	FILTER['C%'] = (FILTER['C'].astype(float)/FILTER['sum'].astype(float))
	FILTER['G%'] = (FILTER['G'].astype(float)/FILTER['sum'].astype(float))
	FILTER['T%'] = (FILTER['T'].astype(float)/FILTER['sum'].astype(float))
	FILTER['A%'] = (FILTER['A'].astype(float)/FILTER['sum'].astype(float))
	FILTER['ins%'] = (FILTER['ins'].astype(float)/FILTER['sum'].astype(float))
	FILTER['insA%'] = (FILTER['insA'].astype(float)/FILTER['sum'].astype(float))
	FILTER['insC%'] = (FILTER['insC'].astype(float)/FILTER['sum'].astype(float))
	FILTER['insT%'] = (FILTER['insT'].astype(float)/FILTER['sum'].astype(float))
	FILTER['insG%'] = (FILTER['insG'].astype(float)/FILTER['sum'].astype(float))
	FILTER['del%'] = (FILTER['del'].astype(float)/FILTER['sum'].astype(float))
	FILTER['delA%'] = (FILTER['delA'].astype(float)/FILTER['sum'].astype(float))
	FILTER['delC%'] = (FILTER['delC'].astype(float)/FILTER['sum'].astype(float))
	FILTER['delT%'] = (FILTER['delT'].astype(float)/FILTER['sum'].astype(float))
	FILTER['delG%'] = (FILTER['delG'].astype(float)/FILTER['sum'].astype(float))

	FILTER['C%'] = FILTER['C%'].map('{:,.3f}'.format)
	FILTER['G%'] = FILTER['G%'].map('{:,.3f}'.format)
	FILTER['T%'] = FILTER['T%'].map('{:,.3f}'.format)
	FILTER['A%'] = FILTER['A%'].map('{:,.3f}'.format)
	FILTER['ins%'] = FILTER['ins%'].map('{:,.3f}'.format)
	FILTER['insA%'] = FILTER['insA%'].map('{:,.3f}'.format)
	FILTER['insT%'] = FILTER['insT%'].map('{:,.3f}'.format)
	FILTER['insG%'] = FILTER['insG%'].map('{:,.3f}'.format)
	FILTER['insC%'] = FILTER['insC%'].map('{:,.3f}'.format)
	FILTER['del%'] = FILTER['del%'].map('{:,.3f}'.format)
	FILTER['delA%'] = FILTER['delA%'].map('{:,.3f}'.format)
	FILTER['delC%'] = FILTER['delC%'].map('{:,.3f}'.format)
	FILTER['delT%'] = FILTER['delT%'].map('{:,.3f}'.format)
	FILTER['delG%'] = FILTER['delG%'].map('{:,.3f}'.format)
	FILTER['max_ins%'] = FILTER[['insA%','insT%','insC%','insG%']].max(axis=1)
	# print(FILTER[['insA%','insT%','insC%','insG%']])
	# print(type( FILTER[['insA%','insT%','insC%','insG%']]))
	FILTER['max_ins_base'] = FILTER[['insA%','insT%','insC%','insG%']].values.argmax(axis=1)#.str[3:-1]
	print(FILTER['max_ins_base'].head(5))
	FILTER['max_ins_base'] = FILTER['max_ins_base'].map({0: "A", 1: "T", 2: "C", 3: "G"})
	print(FILTER['max_ins_base'].head(5))
	FILTER['max_del%'] = FILTER[['delA%','delT%','delC%','delG%']].max(axis=1)
	#print(FILTER['max_ins_base'])
	FILTER['max_del_base'] = FILTER[['delA%','delT%','delC%','delG%']].values.argmax(axis=1)#.str[3:-1]
	FILTER['max_del_base'] = FILTER['max_del_base'].map({0: "A", 1: "T", 2: "C", 3: "G"})

	FILTER.drop_duplicates(subset=['#CHROM','POS','DEPTH'], keep='last')

	#	FILTER =  FILTER[['#CHROM','POS','C%','G%','T%','A%','ins%','del%','sum','DEPTH','GENE',
	#				'exone','length','strand','Transcript_ID','refseq','hgmd',
	#				'selection','filt']].sort_values(by=['#CHROM','POS'],ascending=[True,True])
	FILTER =  FILTER[['#CHROM','POS','C%','G%','T%','A%','ins%', 'insA%', 'insC%','insT%', 'insG%','max_ins%', 'max_ins_base','del%','delA%','delC%','delT%','delG%','max_del%','max_del_base','sum','DEPTH',
	                          'selection']].sort_values(by=['#CHROM','POS'],ascending=[True,True])

	FILTER['sum'] = FILTER['sum'].astype(int)
	FILTER['POS'] = FILTER['POS'].astype(int)
	FILTER['DEPTH'] = FILTER['DEPTH'].astype(int)
	FILTER['selection'] = FILTER['selection'].astype(int)
	#FILTER['filt'] = FILTER['filt'].astype(int)
	# FILTER['exone'] = FILTER['exone'].astype(int)
	# FILTER['length'] = FILTER['length'].astype(int)
	# FILTER['strand'] = FILTER['strand'].astype(int)
	#	FILTER['#CHROM'] = FILTER['#CHROM'].astype(int)

	#FILTER2 = FILTER[FILTER['filt']==1]
	print(FILTER.head(10))

	print ('len FILTER:',len(FILTER))
	print ('FILTER OK!!!')

	file_filtered_intermedio = join(folder_indels,'final_'+name_filter_intermedio)

	FILTER.to_csv(file_filtered_intermedio,sep='\t',index=False)
	return

if __name__=="__main__":

	args = InputPar()
	folder = step.principal_folder(args,step.create_folder(),over='False')
	start = time.time()
	# folder = args.path + 'RESULT/' + args.project + '/'
	folder_map = join(folder,'03-Mapping/')
	folder_pheno = join(folder,'pheno/')
	print(folder)
	#folder = '/home/bioinfo/PROJECT/pacbio/RESULT/19_Jan_2021_8_RPE65/'
	print(folder_map+'*.bam.bai')
	samples = glob.glob(folder_map+'*.bam.bai')
	print(samples)

	############################################à
	# print(set(sam))
	#join(folder_name, 'pheno', panel+'.bed')
	_BED = join(folder_pheno, args.panel+'.bed')
	BED = pd.read_csv(_BED, sep='\t', names=['CHROM','START','END'])

	for _sample in set(samples):
		sample = _sample.split('.bam.bai')[0].split('/')[-1]
		folder_indels = join(folder,'indels/')

		file_to_count = join(folder_indels, sample+'_to_count')
		file_vcf = join(folder_indels,sample+'_samt.vcf')
		if not os.path.exists(folder_indels):
			os.makedirs(folder_indels)

		print(' '.join(['samtools','mpileup',
		    '-Q 0 -q 0 -d10000000 -L 100000 -A', folder_map + sample + '.bam', '>', file_to_count]))

		system(' '.join(['samtools','mpileup',
		    '-Q 0 -q 0 -d10000000 -L 100000 -A', folder_map + sample + '.bam', '>', file_to_count]))

		count = pd.read_csv(file_to_count,sep='\t',header=None,names=['CHROM','POS','info','DEPTH','CALL','quality'])
		print('read')

		count_filt = pd.DataFrame(columns=['CHROM','POS','info','DEPTH','CALL','quality'])

		for index, row in BED.iterrows():
			count_tmp = count[(count['CHROM']==row.CHROM) & (count['POS']<=row.END) & (count['POS']>=row.START)]
			count_filt = pd.concat([count_filt,count_tmp])
		print('subset')
		#print(b.head(10))
		file_to_count2 = join(folder_indels,sample+'_to_count2')
		count_filt.to_csv(file_to_count2,sep='\t',header=None)

		print ('load count!!!')
		count_filt2 = pd.read_csv(file_to_count2,sep='\t',header=None,quoting=csv.QUOTE_NONE,encoding='utf-8',low_memory=False,
		                          error_bad_lines=False,names=['CHROM','POS','info','DEPTH','CALL','quality'],chunksize=40*100024)
		print('chunck')
		folder_to_count = join(folder_indels,'to_count/')
		if not os.path.exists(folder_to_count):
			os.makedirs(folder_to_count)

		i=0
		for chunk in count_filt2:
			print('chunck '+ str(i))
			i+=1
			chunk['sample'] = sample
			count = count_coverage(folder_indels,sample,chunk)
			#print(count)
			name = join(folder_to_count,sample)
			print(name)
			filter_for_panel(folder_indels,name,sample)
			print (i)

		name_filter_intermedio = (sample+'_unbalance_inter.txt')
		file_filtered_intermedio = join(folder_indels,'final_'+name_filter_intermedio)

		file_count_perc = pd.read_csv(file_filtered_intermedio, sep='\t')

		# file_count_perc['mean_max_ins'] = file_count_perc['max_ins%'].mean(axis=0)
		# file_count_perc['mean_max_del']= file_count_perc['max_del%'].mean(axis=0)
		# file_count_perc['stddev_max_ins'] = file_count_perc['max_ins%'].std(axis=0)
		# file_count_perc['stddev_max_del'] = file_count_perc['max_del%'].std(axis=0)
		# print(file_count_perc.head(5))
		#
		# print('mean: ',file_count_perc[['max_ins%','max_del%']].mean(axis=0))
		# print('std dev: ',file_count_perc[['max_ins%','max_del%']].std(axis=0))

		#zscore calculation
		file_count_perc['zscore_max_ins'] = file_count_perc[['max_ins%']].apply(zscore)
		file_count_perc['zscore_max_del'] = file_count_perc[['max_del%']].apply(zscore)


		file_count_perc_zscore_filtered = file_count_perc[file_count_perc['zscore_max_ins']>=5]
		# print(file_count_perc_zscore_filtered)
		zscore = join(folder_indels,sample+'_indel_filter_zscore_5.csv')
		file_count_perc_zscore_filtered.to_csv(zscore,sep='\t', index=False)

		# description
		# print('descr: ')
		# print(file_count_perc[['max_ins%','max_del%','zscore_max_ins']].describe(percentiles=[.25, .5, .75, .95, .99]))
		#file_count_perc[['max_ins%','max_del%','zscore_max_ins']].describe(percentiles=[.25, .5, .75, .95, .99]).to_csv(join(folder_indels,sample+'_describe.csv'), sep='\t')
		# print(file_count_perc.head(5))
		file_count_perc['pvalues_ins'] = st.norm.sf(abs(file_count_perc['zscore_max_ins']))
		file_count_perc['pvalues_ins_prob'] = st.norm.cdf((file_count_perc['pvalues_ins']))
		# print(len(file_count_perc[file_count_perc['pvalues_ins_prob']>0.85]))
		# print(file_count_perc.head(5))
		final_indel = join(folder_indels,sample+'_final_count_perc.csv')
		file_count_perc.to_csv(final_indel, sep='\t', index=False)
		# file_count_perc.hist(columns='zscore_max_ins')
		# file_count_perc.plot()

		# n, bins, patches = plt.hist(file_count_perc[['zscore_max_ins']], 50)
		# print('e')
		# plt.xlabel('zscore')
		# plt.ylabel('freq')
		#
		# r = st.norm.rvs(size=1000)
		# plt.hist(r)
		# print('f')
		# plt.savefig(folder_indels+'hist_zscore_norm')
		# print('g')

		#plot boxplot max_ins
		sns.boxplot(x=file_count_perc['max_ins%'])
		plt.savefig(folder_indels+'boxplot_max_ins.png')
		sns1.boxplot(x=file_count_perc['zscore_max_ins'])
		plt1.savefig(folder_indels+'boxplot_zscore_max_ins.png')

		# print(file_count_perc.head())
		#
		#
		# Q1=file_count_perc[['max_ins%']].quantile(0.25)
		# Q3=file_count_perc[['max_ins%']].quantile(0.75)
		# print('Q1: ',Q1, ' Q3: ', Q3)
		# IQR=Q3-Q1
		# print('IQR: ', IQR)
		# lower_bound=Q1 - 1.5 * IQR
		# upper_bound=Q3 + 1.5 * IQR
		# print('lower bound: ',lower_bound,'upper bound: ',upper_bound)
		# print((upper_bound.values[0]))
		#
		# IQR_1_5_ = join(folder_indels,sample+'_indel_filter_IQR_1.5.csv')
		# file_count_perc[file_count_perc['max_ins%']>=upper_bound.values[0]].to_csv(IQR_1_5_, sep='\t', index=False)
		#
		#
		# lower_bound=Q1 - 2 * IQR
		# upper_bound=Q3 + 2 * IQR
		# print('lower bound: ',lower_bound,'upper bound: ',upper_bound)
		# print((upper_bound.values[0]))
		#
		# IQR_2_ = join(folder_indels,sample+'_indel_filter_IQR_2.csv')
		# file_count_perc[file_count_perc['max_ins%']>=upper_bound.values[0]].to_csv(IQR_2_, sep='\t', index=False)
		#
		# lower_bound=Q1 - 3 * IQR
		# upper_bound=Q3 + 3 * IQR
		# print('lower bound: ',lower_bound,'upper bound: ',upper_bound)
		# print((upper_bound.values[0]))
		#
		# IQR_3_ = join(folder_indels,sample+'_indel_filter_IQR_3.csv')
		# file_count_perc[file_count_perc['max_ins%']>=upper_bound.values[0]].to_csv(IQR_3_, sep='\t', index=False)

		#
		#
		# a = ": mu: {:.2f}, sigma:{:.2f}".format(float(file_count_perc[['max_ins%']].mean(axis=0)), float(file_count_perc[['max_ins%']].std(axis=0)))
		# plt.title('Z-score max_ins% distribution' + a)
		# plt.text(60, .025, a)
		# # plt.xlim(40, 160)
		# # plt.ylim(0, 0.03)
		# plt.grid(True)
		# #plt.show()
		# plt.savefig(folder_indels+'hist_zscore')

		#hist of ins
		# n, bins, patches = plt.hist(file_count_perc[['max_ins%']], 50)
		# plt.xlabel('max_ins%')
		# plt.ylabel('freq')
		#
		#
		# # a = ": mu: {:.2f}, sigma:{:.2f}".format(float(file_count_perc[['max_ins%']].mean(axis=0)), float(file_count_perc[['max_ins%']].std(axis=0)))
		# # plt.title('Z-score max_ins% distribution' + a)
		# # plt.text(60, .025, a)
		# # plt.xlim(40, 160)
		# # plt.ylim(0, 0.03)
		# plt.grid(True)
		# #plt.show()
		# plt.savefig(folder_indels+'hist_ins_count')


		#hist of pvalues_ins_prob
		# n, bins, patches = plt.hist(file_count_perc[['pvalues_ins_prob']], 50)
		# plt.xlabel('pvalues_ins_prob%')
		# plt.ylabel('freq')
		#
		#
		# # a = ": mu: {:.2f}, sigma:{:.2f}".format(float(file_count_perc[['max_ins%']].mean(axis=0)), float(file_count_perc[['max_ins%']].std(axis=0)))
		# # plt.title('Z-score max_ins% distribution' + a)
		# # plt.text(60, .025, a)
		# # plt.xlim(40, 160)
		# # plt.ylim(0, 0.03)
		# plt.grid(True)
		# #plt.show()
		# plt.savefig(folder_indels+'pvalues_ins_prob')

#plot of positions
		fig, ax = plt1.subplots()
		ax.plot(file_count_perc[['POS']], file_count_perc[['max_ins%']])

		ax.set(xlabel='position', ylabel='max_ins',
		       title='max_ins%')
		ax.grid()

		fig.savefig(folder_indels+'hist_ins%')
		plt1.show()




		end = time.time()
		print ("It took {} minutes to execute this".format((end - start) / 60.))
