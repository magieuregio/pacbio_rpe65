#!/home/bioinfo/miniconda3/envs/pacbioenv/bin/python
#27/06/2015

# -*- coding: utf-8 -*-

from multiprocessing import Process, Lock
import multiprocessing
import argparse
from os import listdir, system, getcwd,getenv
from os.path import isfile, join, exists
import datetime
import subprocess
import time
from os import getenv
import time,os,sys
import csv
import glob
import pandas as pd

path = getcwd()
reference = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE')
appris_principal = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','structuralvariant.bed')
bed = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','BED')
def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='MAGI EUREGIO DIAGNOSYS',description='Pipe from FASTQ to BAM',
			epilog='Need to BWA 0.7.12-r1039 and SAMTOOLS v1.3 (SET on global PATH or INSERT your path [--bwa] [--samtools]')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = Pipe run in same folder when launch command]')

	parser.add_argument('-fq','--fastq', metavar='FASTQ',required=True,
			help='FULL PATH to find FASTQ FILE - [Required]')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Pannelli Consigliati: 1:CLM2; 2:OSSNEW; 3:OCULARE; 4:OCULARE2; 5:SIFSR; 6:ALLGENE;'
					'7:trusightone; 8:GENETEST1; 9:GENETEST2; 10:CANCER; 11:MALFORMATIONI VASCOLARI;'
					'12:ANOMALIE VASCOLARI; 13:INFERTILITA; 14:OBESITA; 15:GENODERMATOSI\nRequired')

	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z','p'],
	                    required=True,
	                    help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca; p = privato,  - required ')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert Family name to create Folder')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: ALLERT!!! Set on "False" overwrite old data [Default = True]')

#	parser.add_argument('-o','--name', metavar='Output ', required=True,help='Choose an output filename (Required)')

	return parser.parse_args()


def create_folder():
	today = datetime.date.today()
	name = "{:%d_%b_%Y}".format(today)
	return name

def principal_folder(param, name, over=None):

	if over == 'True':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		try:
			os.makedirs(name_folder)
		except OSError:
			sys.exit("This folder already exists - Please, choose another project name otherwise you can run the pipeline with <-proj> option")
		finally:
			print ('-----------------------------------------')
		return name_folder

	elif over == 'False':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		return name_folder



def path_creation(param,folder):
	print ('built tree folder...')
	spec_fastq = join(folder,'fastq/')
	spec_indel = join(folder,'indels/')
	spec_other = join(folder,'other/')
	spec_pheno = join(folder,'pheno/')
	if not os.path.exists(spec_other):
		os.makedirs(spec_other)
	if not os.path.exists(spec_pheno):
		os.makedirs(spec_pheno)
	if not os.path.exists(spec_indel):
		os.makedirs(spec_indel)
	if not os.path.exists(spec_fastq):
		os.makedirs(spec_fastq)
	return

def copy_fastq(param,folder):
	print ('copy files...')
	fastq_folder = join(folder,'fastq/')
	pheno_folder = join(folder,'pheno/')
	fastqc_folder = join(folder,'fastQC/')
	files_fq = glob.glob(join(param.fastq,'*.fastq'))
	files_bam = glob.glob(join(param.fastq,'*.bam'))

	# sample_list = pd.read_csv(join(param.fastq,'samples.csv'), sep='\t', names=['SAMPLE', 'BC'])
	#m54094_211001_152814.demux.ccs_longerThan13kb.R2479_E12_BC1093--R2479_E12_BC1093.bam
	#m54094_211001_152814.demux.ccs_longerThan13kb.R2458_H9_BC1072--R2458_H9_BC1072.fastq

	if (len(files_fq)!=0):
		print(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder])) #todo decommenta
		pattern_list = pd.DataFrame(files_fq, columns=['fastq'])
		# pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.fastq').str.get(0)
		pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.').str.get(-2).str.split('_').str.get(0)
		pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.').str.get(-2).str.split('_').str.get(0) + '.2021'
		# pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('ccs_longerThan13kb.').str.get(1).str.split('_').str.get(0) + '.2021' #todo decide how to fill this column, from a sample sheet?, mpattern recognition?
		 #todo decide how to fill this column, from a sample sheet?, mpattern recognition?
	elif (len(files_bam)!=0):
		print(' '.join(['cp',join(param.fastq,'*.bam*'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.bam'),fastq_folder])) #todo decommenta
		print(' '.join(['cp',join(param.fastq,'*.bam.pbi'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.bam.pbi'),fastq_folder])) #todo decommenta
		pattern_list = pd.DataFrame(files_bam, columns=['fastq'])
		pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.bam').str.get(0)
		pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('ccs_longerThan13kb.').str.get(1).str.split('_').str.get(0) + '.2021'#todo decide how to fill this column, from a sample sheet?, mpattern recognition?
	else:
		print('Error: nothing found in fastq folder')
		pattern_list = pd.DataFrame(columns=['fastq'])

	print(pattern_list)

	pattern_list = pattern_list.drop('fastq', 1)
	pattern_list = pattern_list[['sample', 'file']]

	pattern_list = pattern_list.drop_duplicates()

	_pattern_list = join(folder, 'patterns.tsv')

	pattern_list.to_csv(_pattern_list, sep='\t', index=False, header = False) ##todo: decommenta

	###copy bed
	_bed = join(path,'BED', panel+'.bed')
	print(' '.join(['cp',_bed, pheno_folder]))
	system(' '.join(['cp',_bed, pheno_folder]))


def create_folder():
	today = datetime.date.today()
	name = "{:%d_%b_%Y}".format(today)
	return name

if __name__=="__main__":
	args = InputPar()

	proj_name = create_folder()
	if args.panel == 'trusightone':
		panel = 'trusightone' #args.panel
	else:
		panel = args.panel.upper()
	if args.dest == 'r':
		dest = 'rovereto'
	elif args.dest == 'b':
		dest = 'bolzano'
	elif args.dest == 's':
		dest = 'sanfelice'
	elif args.dest == 'z':
		dest = 'ricerca'
	elif args.dest == 'p':
		dest = 'privato'

	genome = 'geno38'
	if args.over == 'True': over = 'True'
	elif args.over == 'False': over = 'False'
	if args.project:
		proj_name = args.project
		name_folder= join(path,'RESULT',proj_name+'_'+panel)
		#system(' '.join(['chmod 777 -R',name_folder]))
		print ('\nIL NOME DEL PROGETTO E\': '+proj_name+'_'+panel+' ---> PER: '+dest+'\n')
	else:
		name_folder= join(path,'RESULT',proj_name+'_'+panel)
		print ('\nIL NOME DEL PROGETTO E\': '+proj_name+'_'+panel+' ---> PER: '+dest+'\n')
	print (name_folder+'!!!')

	if ((os.path.exists(name_folder)) & (args.over == 'True')):
		sys.exit("Folder exist just - Choose another project name, please!!! You can run with <-proj> option")
	elif ((os.path.exists(name_folder)) & (args.over == 'False')):
		args.runcore = 'False'
	else: pass
##########################################################################################################################
##########################################################################################################################
	if args.over == 'True':
		folder = create_folder()
		folder_name = principal_folder(args,folder,over=args.over)
		path_creation(args,folder_name)
	elif args.over == 'False':
		folder = create_folder()
		folder_name = principal_folder(args,folder,over=args.over)

    ##copy fastq in folder results
	pheno_folder = join(folder,'pheno/')
	_bed = join(pheno_folder, panel+'.bed')
	# print(folder)
	# print(folder_name)
	copy_fastq(args,folder_name)
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')

	##create config file
	print(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq'), '-b', join(folder_name, 'pheno', panel+'.bed'),
	'-p', join(folder_name, 'patterns.tsv'), '-r', join(reference, 'all_chr38.fa'),'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
	'--PBSVtrf', structural_variants,'--PBSVtypes' ,'DEL,INS,INV,DUP,BND,CNV', '--VEPdbNSFP', '/home/bioinfo/.vep/Plugins/38_version4/dbNSFP4.1a.gz', '--VEPdbscSNV','/home/bioinfo/.vep/Plugins/38_version4/SNV/dbscSNV1.1_GRCh38.txt.gz']))
	#--VEPdbNSFP FILE --VEPdbscSNV /home/bioinfo/.vep/Plugins/38_version4/SNV/dbscSNV1.1_GRCh38.txt.gz
	# ./createConfigFile.py -i /home/bioinfo/PROJECT/pacbio/RESULT/15_Oct_2021_FLG/fastq -b /home/bioinfo/PROJECT/pacbio/RESULT/15_Oct_2021_FLG/pheno/FLG.bed -p /home/bioinfo/PROJECT/pacbio/RESULT/15_Oct_2021_FLG/patterns.tsv -r /home/bioinfo/PROJECT/pacbio/dataset/REFERENCE/all_chr38.fa --VEPspecies homo_sapiens --VEPassembly GRCh38 --VEPappris /home/bioinfo/PROJECT/pacbio/dataset/REFERENCE/APPRIS_PRINCIPAL --PBSVtrf /home/bioinfo/PROJECT/pacbio/dataset/REFERENCE/structuralvariant.bed --PBSVtypes DEL,INS,INV,DUP,BND,CNV --VEPdbNSFP /home/bioinfo/.vep/Plugins/38_version4/dbNSFP4.1a.gz  --VEPdbscSNV /home/bioinfo/.vep/Plugins/38_version4/SNV/dbscSNV1.1_GRCh38.txt.gz

	time.sleep(3)
	system(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq'), '-b', join(folder_name, 'pheno', panel+'.bed'),
	'-p', join(folder_name, 'patterns.tsv'), '-r', join(reference, 'all_chr38.fa'),'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
	'--PBSVtrf', structural_variants,'--PBSVtypes' ,'DEL,INS,INV,DUP,BND,CNV','--PBSVtypes', 'DEL,INS,INV,DUP,BND,CNV', '--VEPdbNSFP', '/home/bioinfo/.vep/Plugins/38_version4/dbNSFP4.1a.gz', '--VEPdbscSNV','/home/bioinfo/.vep/Plugins/38_version4/SNV/dbscSNV1.1_GRCh38.txt.gz'])) #todo:decommenta
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./pbcaller.py', '-i', join(folder_name,'fastq'),  '-r', reference,  '-b', join(folder_name, 'pheno'),'-o', folder_name]))
	time.sleep(3)
	system(' '.join(['./pbcaller.py', '-i', join(folder_name,'fastq'),  '-r', reference,  '-b', join(folder_name, 'pheno'),'-o', folder_name]))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./appris_filtering.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
	time.sleep(3)
	system(' '.join(['./appris_filtering.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./third_analysis.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
	time.sleep(3)
	system(' '.join(['./third_analysis.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./variant_calling_indel.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
	time.sleep(3)
	system(' '.join(['./variant_calling_indel.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d', args.dest]))
