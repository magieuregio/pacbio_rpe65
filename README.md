# Variant calling with PacBio data #

This pipeline can be executed in either your Linux environment (you must install all dependencies) or in Docker. In case you want to run the pipeline in your own Linux environment, you must install the following software using [Conda](https://bioconda.github.io/user/install.html): `fastqc=0.11.9`, `multiqc=1.9`, `pbmm2=1.4.0`, `picard=2.23.8`, `longshot=0.4.1`, `pbsv=2.4.0`, `ensembl-vep=101.0`, `qualimap=2.2.2a`, `samtools=1.7`. In contrast, in order to run the pipeline with Docker, you need first to install [Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository). Once Docker is already installed, you should compile the Docker image by doing `docker build -t pbcaller .` in the directory of this repository (should be done once).

## How to run the pipeline with Docker? ##

Please execute the file `bash wrapper.sh`.

## How to run the pipeline in our own Linux environment? ##

Please execute the file `./pipeline_PacMAGI.py -p [PATH] -d [DB] -pan [PANEL] -fq [FASTQ_FOLDER] -proj [PROJECT_NAME]`.
This script will build the folder of the analysis in the RESULT folder, copying the fastq in the correct folder, and it will call `createConfigFile.py`, `pbcaller.py`,  `appris_filtering.py`, `third_analysis.py` and `variant_calling_indel.py`.

## Available arguments ##

Regardless of whether you are running the pipeline with Docker or using your own Linux environment, the following arguments are available:

```
usage: pbcaller.py [-h] -i DIR -r DIR -b DIR -o DIR [-t INT] [--snpcov INT]
                   [--snpqual INT] [--svcov INT] [-v]

Variant calling for PacBio data

optional arguments:
  -h, --help            show this help message and exit
  -i DIR, --input DIR   Directory that contains the sequencing data as well as
                        the configuration file 'config.json' [required].
  -r DIR, --reference DIR
                        Directory that contains the reference genome
                        [required].
  -b DIR, --bed DIR     Directory that contains the BED file with the target
                        regions [required].
  -o DIR, --output DIR  Output directory to store the results [required].
  -t INT, --threads INT
                        Number of CPUs to use [default 1].
  --snpcov INT          Minimum read coverage for a SNP to be called [default
                        15].
  --snpqual INT         Minimum quality for a SNP to be called [default 30].
  --svcov INT           Minimum read coverage for a SV to be called [default
                        5].
  -v, --verbose         Show detailed information about the processes (e.g.
                        commands) in the standard output.
```

The required arguments are `-r`, which is the directory that contains the uncompressed reference genome in FASTA format (allowed extensions: `.fasta`, `.fa`), `-b`, which is the directory that contains the BED file with the target regions, `-o`, which is the output directory that will contain all results, and `-i`, which is the directory that contains the sequencing data (allowed extensions: `.fastq`, `.fastq.gz`, `.fq`, `.fq.gz`, `.bam`) and the configuration file `config.json`. How this configuration file looks like?

```
{
    "genome": "hg38.fa",
    "target": "targets.bed",
    "PBSVtrf": "genome_prefix.trf.bed",
    "PBSVtypes": "DEL,INS,INV,DUP,BND,CNV",
    "VEPspecies": "homo_sapiens",
    "VEPassembly": "GRCh38",
    "VEPdbNSFP": "dbNSFP4.1.txt.gz",
    "VEPdbscSNV": "dbscSNV.txt.gz",
    "VEPappris": "APPRIS_PRINCIPAL",
    "samples": {
        "s1": [
            "SRR2962989.fastq.gz",
            "SRR2962988.fastq.gz",
            "SRR2962990.fastq.gz"
        ]
    }
}
```

The configuration file is in JSON format and must contain the uncompressed reference FASTA file (e.g. `"genome": "hg38.fa"`), the target BED file (e.g. `"target": "targets.bed"`), the species name (e.g. `"VEPspecies": "homo_sapiens"`), the assembly version (e.g. `"VEPassembly": "GRCh38"`), and the sequencing files (e.g. `SRR2962989.fastq.gz`) for each sample (e.g. `s1`). The species name and the assembly version should be available in Ensembl and are required for the variant annotation step with `Ensembl-VEP`. `Ensembl-VEP` is called with the `--cache` argument; it means the RefSeq cache file should be first downloaded ([from here](ftp://ftp.ensembl.org/pub/release-101/variation/indexed_vep_cache/)), uncompressed and placed into the same directory that contains the reference FASTA file.

In order to call structural variants with pbsv, it is highly recommended to provide one tandem repeat annotation. It can be downloaded from the PacBio site ([file for GRCh38](https://raw.githubusercontent.com/PacificBiosciences/pbsv/master/annotations/human_GRCh38_no_alt_analysis_set.trf.bed)). The filename should be specified in the configuration file as `PBSVtrf` and the file should be placed into the same directory that contains the reference genome. In case `PBSVtrf` is not specified in the configuration file (it is an optional argument), the structural variation calling will be carried out without this information (less accurate).

The argument `PBSVtypes` is optional and indicates what kind of structural variations have to be called. In case it is not provided in the configuration file, all structural variations will be called (deletions DEL, insertions INS, inversions INV, duplications DUP, translocations BND, copy number variations CNV).

`VEPdbNSFP` and `VEPdbscSNV` are optional arguments in the configuration file to indicate the name of the corresponding files for the plugins `dbNSFP` and `dbscSNV` of `Ensembl-VEP`. These files should be placed into the same directory that contains the reference FASTA file. If `VEPdbNSFP` and `VEPdbscSNV` are not specified in the configuration file, `Ensembl-VEP` will be called without these plugins. `VEPappris` is also optional and specifies a tab-separated table with at least 2 columns: the 1st column containing a gene name and the 2nd columns the desired transcript ID to be annotated for that gene. This file should also be placed into the same directory that contains the reference FASTA file. If `VEPappris` is not specified in the configuration file, no filtering will be performed on the annotated VCF files, so all transcripts for a gene will appear in the VCF files.

## Auxiliary script ##

In order to create the configuration in an easy way, the auxiliary script `createConfigFile.py` is available. In order to execute this script, please do: `python3 createConfigFile.py`. This script has the following arguments:

```
usage: createConfigFile.py [-h] -i DIR -p FILE -r FILE -b FILE --VEPspecies
                           STRING --VEPassembly STRING [--VEPdbNSFP FILE]
                           [--VEPdbscSNV FILE] [--VEPappris FILE]
                           [--PBSVtrf FILE] [--PBSVtypes STRING]

Generate the configuration file for the variant calling pipeline with PacBio
data

optional arguments:
  -h, --help            show this help message and exit
  -i DIR, --input DIR   Directory that contains the sequencing data
                        [required].
  -p FILE, --pattern FILE
                        Tab-separated file with with the sample name in the
                        first column and a pattern in the second column. This
                        is meant to merge sequencing data for each sample in
                        case of multiple files [required].
  -r FILE, --reference FILE
                        Reference genome in FASTA format [required].
  -b FILE, --bed FILE   BED file containing the target regions [required].
  --VEPspecies STRING   Species name. It should be the scientific name without
                        spaces (e.g. homo_sapiens) and the species should be
                        available in Ensembl [required].
  --VEPassembly STRING  Assembly name (e.g. GRCh38). It should be available in
                        Ensembl [required].
  --VEPdbNSFP FILE      File for Ensembl-VEP dbNSFP plugin.
  --VEPdbscSNV FILE     File for Ensembl-VEP dbscSNV plugin.
  --VEPappris FILE      APPRIS file for Ensembl-VEP.
  --PBSVtrf FILE        Tandem Repeat annotation File (TRF) for pbsv.
  --PBSVtypes STRING    Structural variants to be called [default
                        DEL,INS,INV,DUP,BND,CNV].
```

The tab-separated file required for `createConfigFile.py` has 2 columns: the first column contains the sample name while the second column contains a pattern in which all files belonging to the same sample have. For instance, the following file...

```
s1  SRR
```

...will consider as sample `s1` all files with `SRR` in their filenames. In case you have multiple patterns for a sample, you could create the the tab-separated file with the different patterns separated by comma:

```
s1  SRR29,SRR30
```

Finally, in case you have just 1 file per sample, you can directly specify the filename as the pattern:

```
s1  SRR2962989
```

## Output/results ##

In the output directory, the following directories will be found:

```
01-Merged_input_data
02-Quality_check
03-Mapping
04-general_statistics
05-Variant_calling
06-Variant_annotation
```

The first directory, `01-Merged_input_data`, will be only created if different sequencing files come from the same sample, so they need to be merged.

Directory `02-Quality_check` will include the output of `FastQC` and `MultiQC`. `FastQC` will generate an HTML file for each sample (e.g. `SampleName_fastqc.html`) with statistics about the number of reads, the quality of reads, and so on. `MultiQC` will generate an aggregated report considering the information reported by `FastQC` into a unique file called `multiqc_report.html`.

Directory `03-Mapping` will contain for each sample a mapping file in BAM format.

Directory `04-general_statistics` will contain for each sample the output of `Picard CollectHsMetrics` in text format (e.g. `SampleName_PicardCollectHsMetrics.txt`) and the output of `Qualimap` in HTML format (e.g. `SampleName_qualimap/qualimapReport.html`). `Picard CollectHsMetrics` will calculate the on-target and off-target percentage of reads and the depth coverage. `Qualimap` will compute general statistics about the mapping step, such as the number of reads mapped or unmapped.

Directory `05-Variant_calling` will contain VCF files from the variant calling step (e.g. `SampleName.longshot.vcf` for SNPs and `SampleName.pbsv.vcf` for SVs). VCF files are filtered according some criteria: 1) a minimum variant coverage (modulable from the command line arguments), 2) the variant is classified as `PASS` according to the caller, and 3) a minimum variant quality (modulable from the command line arguments; only available for SNPs). Filtered VCF files have the extension `.filt.vcf`.

Directory `06-Variant_annotation` contains the output of `Ensembl-VEP` for each filtered VCF file. Only for SNPs, the output of `Ensembl-VEP` is filtered in order to retain the transcripts that appear in the APPRIS file provided; the APPRIS filtered file have the extension `.filt.appris.vcf`.

## Additional notes ##
- In case you want to run the pipeline with Docker, you will need sudo privileges unless your user belongs to the group `docker`.
