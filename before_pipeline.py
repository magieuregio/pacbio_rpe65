#!/home/bioinfo/miniconda3/envs/pacbioenv/bin/python
#27/06/2015

# -*- coding: utf-8 -*-



path = getcwd()
reference = join(path, 'dataset/REFERENCE', 'all_chr38.fa')
appris_principal = join(path, 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join(path, 'dataset/REFERENCE','structuralvariant.bed')

def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='MAGI EUREGIO DIAGNOSYS',description='Pipe from FASTQ to BAM',
			epilog='Need to BWA 0.7.12-r1039 and SAMTOOLS v1.3 (SET on global PATH or INSERT your path [--bwa] [--samtools]')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = Pipe run in same folder when launch command]')

	parser.add_argument('-fq','--fastq', metavar='FASTQ',required=True,
			help='FULL PATH to find FASTQ FILE - [Required]')

	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z','p'],
	                    required=True,
	                    help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca; p = privato,  - required ')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert Family name to create Folder')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: ALLERT!!! Set on "False" overwrite old data [Default = True]')

#	parser.add_argument('-o','--name', metavar='Output ', required=True,help='Choose an output filename (Required)')

	return parser.parse_args()


def create_folder():
	today = datetime.date.today()
	name = "{:%d_%b_%Y}".format(today)
	return name

def principal_folder(param, name, over=None):

	if over == 'True':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		try:
			os.makedirs(name_folder)
		except OSError:
			sys.exit("This folder already exists - Please, choose another project name otherwise you can run the pipeline with <-proj> option")
		finally:
			print ('-----------------------------------------')
		return name_folder

	elif over == 'False':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		return name_folder



def path_creation(param,folder):
	print ('built tree folder...')
	spec_fastq = join(folder,'fastq/')
	spec_indel = join(folder,'indels/')
	spec_other = join(folder,'other/')
	spec_pheno = join(folder,'pheno/')
	if not os.path.exists(spec_other):
		os.makedirs(spec_other)
	if not os.path.exists(spec_pheno):
		os.makedirs(spec_pheno)
	if not os.path.exists(spec_indel):
		os.makedirs(spec_indel)
	if not os.path.exists(spec_fastq):
		os.makedirs(spec_fastq)
	return

def copy_fastq(param,folder):
	print ('copy files...')
	fastq_folder = join(folder,'fastq/')
	pheno_folder = join(folder,'pheno/')
	fastqc_folder = join(folder,'fastQC/')
    files_fq = glob.glob(join(param.fastq,'*'))

	if len(files_fq)!=0:
        print(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder]))
    else:
        print('Error: nothing in fastq folder')

    pattern_list = pd.DataFrame()
    pattern_list['samples'] = files_fq.str.split('.fastq').str.get(0)

    _pattern_list = join(folder, 'patterns.tsv')

    pattern_list.to_csv(_pattern_list, sep='\t')



if __name__=="__main__":
	args = InputPar()


    if args.over == 'True':
    	folder = create_folder()
    	folder_name = principal_folder(args,folder,over=args.over)
    	path_creation(args,folder_name)
    elif args.over == 'False':
    	folder = create_folder()
    	folder_name = principal_folder(args,folder,over=args.over)

    ##copy fastq in folder results
    copy_fastq(args,folder)

    ##create config file
    print(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq/'), '-b', join(folder_name, 'pheno', 'targets.bed'),
    '-p' join(folder_name, 'patterns.tsv'), '-r', reference,'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
    '--PBSVtrf', structural_variants,'--PBSVtypes', ,'DEL,INS,INV,DUP,BND,CNV']))
    time.sleep(3)
    system(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq/'), '-b', join(folder_name, 'pheno', 'targets.bed'),
    '-p' join(folder_name, 'patterns.tsv'), '-r', reference,'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
    '--PBSVtrf', structural_variants,'--PBSVtypes', ,'DEL,INS,INV,DUP,BND,CNV']))
