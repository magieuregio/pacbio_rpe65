#!/home/remo/miniconda3/envs/pacbioenv/bin/python3

#importing Python required modules
# from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
# from varsome_api.models.variant import AnnotatedVariant
import json
import pandas
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json


def varsome_verdict(input):
	acmg_verdict = ''
	acmg_annotation = ''
	explanation = ''
	verdict = ''

	#NM_000329.3:c.1128+216T>C
	#NM_000329.3:c.1128T>C
	#https://stable-api.varsome.com/lookup/NM_000329:c.1128+216T>C/hg38?add-all-data=0&add-ACMG-annotation=1
	url2 = "/".join(('https://api.varsome.com/lookup',''.join((input,'/hg38?add-all-data=0&add-ACMG-annotation=1'))))
	print(url2)
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url2, headers=headers)
	#print(resp.status_code)
	if resp.status_code==200:
		data = json.loads(resp.text)
		#print (type(data))
		#print(data['acmg_annotation']['verdict']['ACMG_rules']['verdict'])
		_type_ = str((type(data)))
		if _type_ == '<class \'dict\'>':
			data = data
		else:
			data = data[0]
		if 'acmg_annotation' in data.keys():

			classification = []
			acmg_verdict = data['acmg_annotation']['verdict']['ACMG_rules']['verdict']
			acmg_annotation = data['acmg_annotation']['classifications']

			for rule in acmg_annotation:
				# print('in')
				# print(acmg_annotation)
				p = rule['name']
				classification.append((p, rule['user_explain'][0]))
			verdict = str(acmg_verdict)
			#print(verdict)
		try: explanation = str(classification)
		except: explanation = 'Exception da Verificate!!!'
	else:
		verdict = 'Unknown'
		explanation = 'Unknown'
	return (verdict, explanation)
