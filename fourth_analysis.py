#!/home/bioinfo/VIRTUAL3/bin/python3.5
#24/08/2020

# -*- coding: utf-8 -*-
"""
Created on Wed OCT 16 2020

@author: Giuseppe Marceddu
"""

# Load libraries
import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join
import first_diagnosys_core_V2 as step

import numpy as np
import pandas as pd
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
from os import listdir, system
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json

path = os.getcwd()
geno37 = join('/home/bioinfo/','dataset/GENOME/37/all_chr37.fa')
geno38 = join('/home/bioinfo/','dataset/GENOME/38/all_chr38.fa')

#all_genes_exons37 = join('/home/bioinfo/','dataset/MAGIS/37/new15nt_all_genes_exons.txt')
#all_genes_exons38 = join('/home/bioinfo/','dataset/MAGIS/38/new15nt_all_genes_exons.txt')

HGMD_37 = join('/home/bioinfo/','dataset/HGMD/37/HGMD_pro_2014_2.bed')
HGMD_38 = join('/home/bioinfo/','dataset/HGMD/38/LiftOverHGMD_pro_2014_2.bed')

pd.options.display.float_format = '{:,.5f}'.format


def InputPar():
	parser = argparse.ArgumentParser(prog= 'MAGI EUREGIO DIAGNOSYS',description='Pipe coverage analysis')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
						help='[Default = Pipe run in same folder when launch command]')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME',
							help='Insert Family name to create Folder')

	parser.add_argument('-g','--genome', metavar='choose Assembling', choices=['geno37','geno38'],
						default='geno38',
						help='Choices: geno37, geno38 -Run Analysis with genome GRCh38 or GRCh37')

	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z'],
						required=True,
						help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca,  - required ')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Pannelli Consigliati: 1:CLM2; 2:OSSNEW; 3:OCULARE; 4:OCULARE2; 5:SIFSR; 6:ALLGENE;'
					'7:trusightone; 8:GENETEST1; 9:GENETEST2; 10:CANCER; 11:MALFORMATIONI VASCOLARI;'
					'12:ANOMALIE VASCOLARI; 13:INFERTILITA; 14:OBESITA; 15:GENODERMATOSI\nRequired')

	parser.add_argument('-ovr','--over', metavar='choose Assembling', choices=['True','False'],
							default='False',
							help='Choices: True if you want create new Folder, False to write in just exist folder')

	return parser.parse_args()
#############################################################################
# load and prepare omim file
def load_omim(sospetti_omim):
	SOSPETTI=pd.read_csv(sospetti_omim,sep='\t',header=0)
	SOSPETTI.fillna('Unkwnow',inplace=True)
	new=SOSPETTI["GENE (geneMimNumber)"].str.split(" ", n = 1, expand = True)
	# making separate first name column from new data frame
	SOSPETTI["GENE"]= new[0]
	# making separate last name column from new data frame
	SOSPETTI["geneMimNumber"]= new[1]
	SOSPETTI.drop(columns =["GENE (geneMimNumber)"], inplace = True)

	return SOSPETTI

def load_SAMPLE_data(temp0,temp1):
	SAMPLE=pd.merge(temp0,temp1, on=['sample', 'HGVS', 'CHROM', 'POS', 'GENE','ID'], how='outer', indicator=True)

	SAMPLE.loc[:,'GENE'] = SAMPLE['ALTGENE']
	#SAMPLE=SAMPLE[SAMPLE['_merge']=='both']
	SAMPLE.drop('_merge', axis=1, inplace=True)
	name=SAMPLE_to_analyze[0].split('/')[-1]
	name=name.split('.')[0]+'.'+name.split('.')[1]
	name=name.split('_')[0]
	# ds=SAMPLE_DS[SAMPLE_DS['Sample']==name]['ds']
	SAMPLE.loc[:,'decisionmaf'] = SAMPLE['decisionmaf'].astype(float)
	return SAMPLE

def invert_MAF(SAMPLE,cols1, t=0.9):
	if len(SAMPLE) >=1:
		SAMPLE['MAX_MAF'] = SAMPLE['decisionmaf']
		SAMPLE.loc[(SAMPLE['MAX_MAF']>=t) & (SAMPLE['MAX_MAF']!=1),'comments']='MAF>90%: inversion applied'
		SAMPLE.loc[(SAMPLE['MAX_MAF']>=t) & (SAMPLE['MAX_MAF']!=1),'MAX_MAF']=1-SAMPLE.loc[SAMPLE['MAX_MAF']>=t,'MAX_MAF']
		#SAMPLE = SAMPLE[(SAMPLE['comments']!='MAF>90%: inversion applied')] #& (SAMPLE['unbalance'].str.split('=').str.get(1).astype(float)<=0.95)]
	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def cut_MAF(SAMPLE,cols1,t=0.03):
	#print (SAMPLE[SAMPLE['HGVS']=='chr1:197328873-197328873:T/A'][['HGVS','decisionmaf','decisionINFO','MAF<20']],'11111')
	if len(SAMPLE) >=1:
		SAMPLE.loc[((SAMPLE['decisionmaf']<=t) & (SAMPLE['decisionINFO'] != 'ALL')),'MAF<20']=1
		SAMPLE.loc[((SAMPLE['decisionmaf']>t) & (SAMPLE['decisionINFO'] != 'ALL')  & (SAMPLE['MAF<20'] != 1)),'MAF<20']=0
		SAMPLE.loc[((SAMPLE['decisionmaf']>t) & (SAMPLE['decisionINFO'] != 'ALL')  & (SAMPLE['MAF<20'] != 1)),'Description']='DISCARDED: MAF > 3%'
		# Include in the analysis variants with  unknown MAF
		SAMPLE['MAF<20'].fillna(value=1, inplace=True)
		#print  (SAMPLE[SAMPLE['HGVS']=='chr1:197328873-197328873:T/A'][['HGVS','decisionmaf','decisionINFO','MAF<20']],'22222')
		SAMPLE = SAMPLE[SAMPLE['MAF<20']==1]
		#print  (SAMPLE[SAMPLE['HGVS']=='chr1:197328873-197328873:T/A'][['HGVS','decisionmaf','decisionINFO','MAF<20']],'33333')
	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def include_exceptions(SAMPLE, exception):
	SAMPLE['fromFILE'] = 0
	exceptions=pd.read_csv(exception)

	for index, row in exceptions.iterrows():
		if row.HGVS in list(SAMPLE['HGVS']):
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'MAF<20']=1
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'Description']='SELECTED'
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'MAF_selection']=1
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'comments']='Selected from exception'
	return SAMPLE

def MAF_t(SAMPLE,SOSPETTI,genes,ds,PREV_MAF,PREV,cols1):
	SAMPLE['MAF_selection']=0
	SAMPLE['MAF_selection_AD']=0
	SAMPLE['MAF_selection_AR']=0
	if len(SAMPLE) >=1:
		SAMPLE.loc[:,'controls_nhomalt'] = SAMPLE['gnomAD_exomes_controls_nhomalt'].astype(float)+SAMPLE['gnomAD_genomes_controls_nhomalt'].astype(float)
		mask1 = SAMPLE['controls_nhomalt'] == -1998.0
		mask2 = SAMPLE['controls_nhomalt'] == -999.0
		SAMPLE.loc[mask1,'controls_nhomalt'] = 0
		SAMPLE.loc[mask2,'controls_nhomalt'] = 0
		SAMPLE.reset_index(drop=True, inplace=True)
		for index, row in SAMPLE.iterrows():
				#SAMPLE['allinheritance']=''
				#if row['MAF<20']==1:
				if row['GENE'] in genes:
					inheritance=SOSPETTI[SOSPETTI['GENE']==row['GENE']]['phenotypeInheritance'].unique()
					_allphenotype_ = SOSPETTI[SOSPETTI['GENE']==row['GENE']]['phenotype (phenotypeMimNumber)'].unique()
					try: prevalence=PREV[PREV['Sospetto diagnostico']==ds.upper()]['Prevalenza'].item()
					except: prevalence = '1:2000'
					temp=PREV_MAF[PREV_MAF['Frequenza']==prevalence]
					try:
						if (isinstance(inheritance[0], str)) and (inheritance[0].find('X-linked')==-1):
							if row['MAX_MAF']<=temp[temp['Ereditarietá ']=='AD']['MAF'].item():
								SAMPLE.loc[index,'MAF_selection_AD']=1
							if row['MAX_MAF']<=temp[temp['Ereditarietá ']=='AR']['MAF'].item():
								SAMPLE.loc[index,'MAF_selection_AR']=1
						elif (isinstance(inheritance[0], str)) and (inheritance[0].find('X-linked')!=-1):
							n1=float(prevalence.split(':')[0])
							n2=float(prevalence.split(':')[1])
							if row['MAX_MAF']<=(n1/n2):
								SAMPLE.loc[index,'MAF_selection_AD']=1
								SAMPLE.loc[index,'MAF_selection_AR']=1
								SAMPLE.loc[index,'comments']='X-linked'
						else:
							SAMPLE.loc[index,'comments']='hereditary model is missing!'
							if row['MAX_MAF']<=temp[temp['Ereditarietá ']=='AR']['MAF'].item():
								SAMPLE.loc[index,'MAF_selection_AR']=1
					except:
						SAMPLE.loc[index,'comments']='hereditary model is missing! Not Gene in Data!'
						if row['MAX_MAF']<=temp[temp['Ereditarietá ']=='AR']['MAF'].item():
							SAMPLE.loc[index,'MAF_selection_AR']=1
					SAMPLE.loc[index,'allinheritance'] = str(sorted(inheritance))
					SAMPLE.loc[index,'allphenotype'] = str(sorted(_allphenotype_))

					for item in sorted(inheritance):
						if not item == 'None':
							if 'Autosomal dominant' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AD'])
								SAMPLE.loc[index,'inheritance']= 'AD'
								SAMPLE.loc[index,'prevalence'] = temp[temp['Ereditarietá ']=='AD']['MAF'].item()
							elif 'X-linked' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AR'])
								SAMPLE.loc[index,'inheritance']= 'XL'
								SAMPLE.loc[index,'prevalence'] = temp[temp['Ereditarietá ']=='AR']['MAF'].item()
							elif 'Autosomal recessive' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AR'])
								SAMPLE.loc[index,'inheritance']= 'AR'
								SAMPLE.loc[index,'prevalence'] = temp[temp['Ereditarietá ']=='AR']['MAF'].item()
								break
							else:
								SAMPLE.loc[index,'inheritance']= np.nan
					SAMPLE.loc[(SAMPLE['MAF_selection']==0) & (pd.isnull(SAMPLE['Description'])),'Description']='MAF above threshold'
				else:
					print('errore gene')
	else: SAMPLE = pd.DataFrame(columns=cols1)

	try:
		SAMPLE['inheritance'].fillna('AR',inplace=True)
		SAMPLE['inheritance'].fillna(0.02200,inplace=True)
	except:
		SAMPLE['inheritance'] = 'AR'
		SAMPLE['prevalence'] = 0.02200

	return SAMPLE

def exclude_problemregion(SAMPLE,regioniproblematiche):
	SAMPLE['fromFILE'] = 1
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']
	PROBLEMATICA = regioniproblematiche[regioniproblematiche['tipo']== 'PROBLEMATICA']
	NONREFERTABILE = regioniproblematiche[regioniproblematiche['tipo']== 'NON REFERTABILE']
	OMOLOGIA = regioniproblematiche[regioniproblematiche['tipo']=='OMOLOGIA']
	FUNCTIONALBENIGN = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('benigna'))]
	FUNCTIONALPATHOG = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('patogenetica'))]

	for index, row in PROBLEMATICA.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='NOT SELECTED'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']='Reg. Problematica from FILE'

	for index, row in NONREFERTABILE.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			#print (START,END,int(str(hgvs).split(':')[1].split('-')[0]), ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)))
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='NOT SELECTED'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']= 'Non Refertabile from FILE'
	#### ANCORA DA TESTARE!!!###
	for index, row in OMOLOGIA.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='OMOLOGIA'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']='Omologia from FILE'

	for index, row in FUNCTIONALBENIGN.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE['HGVS'].str.split(':').str.get(1)):
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'Description']='BENIGNA'
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'fromFILE']=0
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'comments']= 'Benigna from FILE'

	for index, row in FUNCTIONALPATHOG.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE['HGVS'].str.split(':').str.get(1)):
			#print (SAMPLE[['HGVS','MAF<20']],'AAAAAAAAAAA')
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'MAF<20']=1
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'Description']='SELECTED'
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'fromFILE']=1
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'comments']='Patogenetica from FILE'
			#print (SAMPLE[['HGVS','MAF<20']],'BBBBBBBBBBBB')
	return SAMPLE

def make_predictions(SAMPLE,traduttore,traduttore2,PRED,cols1):
	SAMPLE['predictors_decision'] = 'NULL'
	SAMPLE['predictors_B'] = 'NULL'
	SAMPLE['predictors_D'] = 'NULL'

	if len(SAMPLE) >=1:
		P_TABLE=pd.DataFrame()
		for predictor in PRED:
			if predictor+'_pred' in SAMPLE.columns:
				P_TABLE[predictor+'_result']=0
				for index, row in SAMPLE.iterrows():
					item=str(row[predictor+'_pred']).split('&')[0]
					r1 = re.findall(r"^\w",str(row[predictor+'_pred']))
					r2 = re.findall(r"^.&\w",str(row[predictor+'_pred']))

					if r1: item = r1[0]
					elif r2: item = r2[0].split('&')[1]
					else: item = 'n'

					if not str(item): P_TABLE.loc[index,predictor+'_result']='NULL'
					elif str(item)=='nan': P_TABLE.loc[index,predictor+'_result']='NULL'
					elif str(item)=='n': P_TABLE.loc[index,predictor+'_result']='NULL'
					else:
						if predictor == 'MutationTaster':
							if traduttore2[item]=='D': P_TABLE.loc[index,predictor+'_result']='D'
							elif traduttore2[item]=='B': P_TABLE.loc[index,predictor+'_result']='B'
							else: P_TABLE.loc[index,predictor+'_result']='NULL'
						else:
							if traduttore[item]=='D': P_TABLE.loc[index,predictor+'_result']='D'
							elif traduttore[item]=='B': P_TABLE.loc[index,predictor+'_result']='B'
							else: P_TABLE.loc[index,predictor+'_result']='NULL'
			else:
				P_TABLE[predictor+'_result'] = SAMPLE[predictor+'_rankscore'].astype(float)
				SAMPLE[predictor+'_rankscore']=SAMPLE[predictor+'_rankscore'].astype(float)
				SAMPLE.loc[SAMPLE[predictor+'_rankscore']==float(-999),predictor+'_rankscore']=-999

				if predictor == 'REVEL':
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.7),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.7),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'
				elif predictor == 'CADD':
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.6),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.6),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'
				else:
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.6),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.6),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'

		for index,row in P_TABLE.iterrows():
			benign = 0
			deleterius = 0
			null = 0

			counts = pd.DataFrame(row.value_counts()).T
			try: deleterius=counts['D'].values[0]
			except: deleterius=0
			try: benign=counts['B'].values[0]
			except: benign=0
			try: null=counts['NULL'].values[0]
			except: null=0

			total = deleterius+benign+null
			if int(total)==int(len(PRED)):
				SAMPLE.loc[index,'predictors_B']=(str(benign)+'/'+str(total))
				SAMPLE.loc[index,'predictors_D']=(str(deleterius)+'/'+str(total))
			else: print ('NON TORNANO I CONTI DEI PREDITTORI!!! Verificare!!!')
		SAMPLE.loc[(SAMPLE['predictors_D'].str.split('/').str.get(0).astype(int)>=10),'predictors_decision']='D'
		SAMPLE.loc[(SAMPLE['predictors_B'].str.split('/').str.get(0).astype(int)>=10),'predictors_decision']='B'

	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def query_varsome_saphetor(input,input2,SAMPLE,index):
	print('query_varsome_saphetor')
	acmg_verdict = ''
	acmg_classificatins = ''
	publications = ''
	clinvar2 = ''
	saphetor = ''
	# #rf_score = -999
	# #ada_score = -999
	rf_score = np.nan
	ada_score = np.nan
	definitivemaf2 = None

	#url2 = "/".join(('https://api.varsome.com/lookup',''.join((input,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	url2 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url2, headers=headers)
	#print(resp.status_code)
	#print('url2')
	if resp.status_code!=200:
		url3 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input2,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
		headers = CaseInsensitiveDict()
		headers["Accept"] = "application/json"
		headers["user-agent"] = "VarSomeApiClientPython/2.0"
		headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
		resp = requests.get(url3, headers=headers)
	#print('url3')
	# if resp.status_code!=200:
	# 	url4 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input3,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	# 	headers = CaseInsensitiveDict()
	# 	headers["Accept"] = "application/json"
	# 	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	# 	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	# 	resp = requests.get(url4, headers=headers)
	if resp.status_code==200:
		#print('in')
		data = json.loads(resp.text)
		#print (type(data))
		al_freq = {}
		_type_ = str((type(data)))
		if _type_ == '<class \'dict\'>':
			data = data
		else:
			data = data[0]
		if 'acmg_annotation' in data.keys():
			classification = []
			acmg_verdict = data['acmg_annotation']['verdict']['ACMG_rules']['verdict']
			acmg_classificatins = data['acmg_annotation']['classifications']
			for rule in acmg_classificatins:
				p = rule['name']
				classification.append((p, rule['user_explain'][0]))
		if 'publications' in data.keys():
			publications = data['publications']
		if 'ncbi_clinvar2' in data.keys():
			clinvar_dict = []
			for k in data['ncbi_clinvar2'][0].keys():
				val = data['ncbi_clinvar2'][0][k]
				clinvar_dict.append((k, val))
			clinvar2 = clinvar_dict
		if 'saphetor_known_pathogenicity' in data.keys():
			saphetor = data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']
		if 'dbnsfp_dbscsnv' in data.keys():
			#print(data['dbnsfp_dbscsnv'][0])
			if 'rf_score' in data['dbnsfp_dbscsnv'][0].keys():
				if (data['dbnsfp_dbscsnv'][0]['rf_score'] is None) == False:
					rf_score = data['dbnsfp_dbscsnv'][0]['rf_score'][0]
					rf_score = float(rf_score)
					#print(rf_score)
			if 'ada_score' in data['dbnsfp_dbscsnv'][0].keys():
				if (data['dbnsfp_dbscsnv'][0]['ada_score'] is None) == False:
					ada_score = data['dbnsfp_dbscsnv'][0]['ada_score'][0]
					ada_score = float(ada_score)
		if 'gnomad_exomes' in data.keys():
			print('gnomad')
			if 'af' in data['gnomad_exomes'][0].keys():
				tot_af = data['gnomad_exomes'][0]['af']
				#nm['af'] = data['gnomad_exomes'][0]['af']
			else:
				tot_af = None
			if 'an' in data['gnomad_exomes'][0].keys():
				tot_an = data['gnomad_exomes'][0]['an']
			else:
				tot_an = None
			if 'ac_afr' in data['gnomad_exomes'][0].keys():
				al_freq['afr'] = {'ac': data['gnomad_exomes'][0]['ac_afr'], 'an': data['gnomad_exomes'][0]['an_afr']}
			if 'ac_amr' in data['gnomad_exomes'][0].keys():
				al_freq['amr'] = {'ac': data['gnomad_exomes'][0]['ac_amr'], 'an': data['gnomad_exomes'][0]['an_amr']}
			if 'ac_eas' in data['gnomad_exomes'][0].keys():
				al_freq['eas'] = {'ac': data['gnomad_exomes'][0]['ac_eas'], 'an': data['gnomad_exomes'][0]['an_eas']}
			if 'ac_nfe' in data['gnomad_exomes'][0].keys():
				al_freq['nfe'] = {'ac': data['gnomad_exomes'][0]['ac_nfe'], 'an': data['gnomad_exomes'][0]['an_nfe']}
			if 'ac_sas' in data['gnomad_exomes'][0].keys():
				al_freq['sas'] = {'ac': data['gnomad_exomes'][0]['ac_sas'], 'an': data['gnomad_exomes'][0]['an_sas']}
			al_freq = pd.DataFrame.from_dict(al_freq, orient='index')
			if 'ac' in al_freq.keys():
				al_freq['af'] = al_freq['ac']/al_freq['an']
			if 'af' in al_freq.keys():
				print('af')
				#nm['max_maf'] = al_freq['af'].max()
				#max_maf = al_freq['af'].max()
				an = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,0][0]
				ac = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,1][0]
				al_freq = al_freq[al_freq.an>=2000]
				maf = al_freq['af'].max() #nm['max_maf_ba1']
				# if max_maf!=maf:
				# 	nm['comments'] = 'Not taken the maximum pop MAF because it did not have at least 2000 alleles'
				definitivemaf2 = maf
				definitivemaf2_description = 'VARSOME gnomAD Exome pop MAF'
				definitivemaf2_info = 'VARSOME_popmax'
			else:
				print('allelefreq gen')
				#nm['max_maf'] = None
				ac = None
				an = None
				definitivemaf2 = tot_af
				definitivemaf2_description = 'VARSOME gnomAD Exome Allele Frequency'
				definitivemaf2_info = 'VARSOME_gnomAD'
		else:
			#print('else2')
			#nm['max_maf'] = None
			tot_af = None
			ac = None
			an = None
			tot_an = None
			maf = None
			definitivemaf2 = None
			definitivemaf2_description = None
			definitivemaf2_info = None
		# if key == 'gerp':
		# 		try: print (value,'AAAAAAAAA')
		# 		except: pass
	SAMPLE.loc[index, 'acmg_verdict'] = str(acmg_verdict)
	#print('out')

	try: SAMPLE.loc[index, 'acmg_classificatins'] = str(classification)
	except: SAMPLE.loc[index, 'acmg_classificatins'] = 'Exception da Verificate!!!'
	print(definitivemaf2)
	print(definitivemaf2_info)
	SAMPLE.loc[index, 'definitivemaf2'] = definitivemaf2
	print(definitivemaf2_description)
	SAMPLE.loc[index, 'decisionINFO'] = definitivemaf2_info
	print(str(SAMPLE.loc[index, 'comments']))
	if definitivemaf2_description != None:
		if str(SAMPLE.loc[index, 'comments']) != 'nan':
			#print('in comments')
			#print((SAMPLE.loc[index, 'comments']))
			SAMPLE.loc[index, 'comments'] = str(SAMPLE.loc[index, 'comments']) + ', ' + definitivemaf2_description
		else:
			SAMPLE.loc[index, 'comments'] = definitivemaf2_description
	#print((SAMPLE.loc[index, 'comments']))
	SAMPLE.loc[index, 'definitivemaf2'] = np.where(definitivemaf2 == None, SAMPLE.loc[index,'decisionmaf'], definitivemaf2 )
	#SAMPLE.loc[index, 'definitivemaf2'] = np.where(SAMPLE.loc[index,'definitivemaf2'] == -999, SAMPLE.loc[index,'definitivemaf'], SAMPLE.loc[index,'definitivemaf2'])
	SAMPLE.loc[index, 'decisionINFO'] = np.where(definitivemaf2 == None, SAMPLE.loc[index, 'decisionINFO'] , definitivemaf2_info )
	print(SAMPLE.loc[index, 'definitivemaf2'])

	SAMPLE.loc[index, 'decisionmaf'] = SAMPLE.loc[index, 'definitivemaf2']

	SAMPLE.loc[index, 'publications'] = str(publications)
	SAMPLE.loc[index, 'clinvar2'] = str(clinvar2)
	SAMPLE.loc[index, 'saphetor'] = str(saphetor)
	SAMPLE.loc[index,'ada_score'] = float(ada_score)
	SAMPLE.loc[index,'rf_score'] = float(rf_score)
	return SAMPLE

def query_varsome_gene(GENE,SAMPLE,index):
	#url = "/".join(('https://api.varsome.com/lookup/gene', GENE, 'hg38?add-source-databases=gnomad_genes'))
	url = "/".join(('https://stable-api.varsome.com/lookup/gene', GENE, 'hg38?add-source-databases=gnomad_genes'))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url, headers=headers)
	try:
		data = json.loads(resp.text)
		data_df = json_normalize(data['gnomad_genes']['items'])
		mis_z = data_df['mis_z'].astype(float)
		lof_z = data_df['lof_z'].astype(float)
	except:
		mis_z = -999
		lof_z = -999
	SAMPLE.loc[index,'mis_z'] = float(mis_z)
	SAMPLE.loc[index,'lof_z'] = float(lof_z)
	return SAMPLE

def search_variants_in_region(chr, start, end):
	length = end-start
	#url = "/".join(('https://api.varsome.com/region_variants/hg38',chr[3:], start,''.join((length,'?add-all-data=1'))))
	url = "/".join(('https://stable-api.varsome.com/region_variants/hg38',chr[3:], start,''.join((length,'?add-all-data=1'))))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url, headers=headers)
	if resp.status_code==200:
		data = json.loads(resp.text)
		data_df = json_normalize(data)
		cols = data_df.columns
		ids=[]
		#leggi dal risultato di varsome tutti i VarsomeID trovati nella regione
		for c in range(0,len(cols)):
			for m, n in data_df[cols[c]].items():
				for e in data_df[cols[c]][m]:
					for m, n in e.items():
						if m == 'variant_id':
							ids.append(n)
		ids = list(set(ids))
	else:
		print('Varsome does not answer')
	return ids

def search_info_for_variant(id):
	#url2 = "/".join(('https://api.varsome.com/lookup',''.join((id,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts&add-ACMG-annotation=1'))))
	url2 = "/".join(('https://stable-api.varsome.com/lookup',''.join((id,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts&add-ACMG-annotation=1'))))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url2, headers=headers)
	if resp.status_code==200:
		data = json.loads(resp.text)
	else:
		data = pd.DataFrame()
	return data

def query_varsome_variants_in_gene(SAMPLE, path, GENE):
	print('Function query_varsome_variants_in_gene')
	#open bad and get regions
	sample = SAMPLE.iloc[0,0]
	file_path = ''.join((path, 'bed_', sample))
	bed = pd.read_csv(file_path, sep = '\t')
	bed_filt = bed[bed.GENE.eq(GENE)]
	res = {}
	i=0
	#per ogni riga del bad file (region)
	for index, row in bed_filt.iterrows():
		chr = str(row['#CHROM'])
		if chr == 'chrX':
			chr =  'chr23'
		elif chr == 'chrY':
			chr =  'chr24'
		start = str(row['START'])
		end = str(row['END'])
		length = str(row['length'])
		refseq = str(row['refseq'])
		exone = ''.join(['EX',str(row['exone'])])
		info = ':'.join([GENE, refseq, exone]) #gene:refseq:exone

		# cerca la regione su varsome, cerca tutte le varianti nella regione indicata dal bad
		# url = "/".join(('https://api.varsome.com/region_variants/hg38',chr[3:], start,''.join((length, '?add-all-data=1'))))#length

		#Broad gnomAD exomes.2.1.1
		#Broad gnomAD genomes.3
		#Genomenon Mastermind.2020.10.02
		#SB kaviar3.4-Feb-2016
		#NCBI ClinVar2.09-Nov-2020
		#NCBI dbSNP.build 154
		#NIH GDC.27
		#Saphetor KnownPathogenic.07-Dec-2020
		#UMICH Bravo.Freeze8

		url = "/".join(('https://stable-api.varsome.com/region_variants/hg38',chr[3:], start,''.join((length, '?add-all-data=1'))))#length
		#url = "/".join(('https://stable-api.varsome.com/ /hg38',chr[3:], start,''.join((length, 'add-source-databases=Broad gnomAD genomes.3'))))#length
		headers = CaseInsensitiveDict()
		headers["Accept"] = "application/json"
		headers["user-agent"] = "VarSomeApiClientPython/2.0"
		headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
		resp = requests.get(url, headers=headers)
		if (resp.status_code==200) & (len(resp.text)!=0):
			data = json.loads(resp.text)
			data_df = json_normalize(data)
			cols = data_df.columns
			ids=[]

			data_df2 = pd.DataFrame(data_df[cols[1]][0]).drop_duplicates(subset='variant_id')
			if len(bed_filt) < 20: data_df2 = data_df2
			elif len(bed_filt) > 50: data_df2 = data_df2.iloc[:2,:]
			else: data_df2 = data_df2.iloc[:5,:]

			for index, row in data_df2.iterrows():
				id = row.variant_id
				#url2 = "/".join(('https://api.varsome.com/lookup',''.join((id,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts&add-ACMG-annotation=1'))))
				url2 = "/".join(('https://stable-api.varsome.com/lookup',''.join((id,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts&add-ACMG-annotation=1'))))
				headers = CaseInsensitiveDict()
				headers["Accept"] = "application/json"
				headers["user-agent"] = "VarSomeApiClientPython/2.0"
				headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
				resp = requests.get(url2, headers=headers)
				if resp.status_code==200:
					data = json.loads(resp.text)
					nm={}
					al_freq = {}
					#da varsome seleziona verdetto, cromosoma, ALT, REF, posizione
					nm['verdict'] = data['acmg_annotation']['verdict']['ACMG_rules']['verdict']
					nm['chr'] = data['chromosome']
					nm['alt'] = data['alt']
					nm['pos'] = data['pos']
					nm['varsomeid'] = id
					nm['hgvs_alt'] = ':'.join([data['chromosome'], '-'.join([str(data['pos']), data['alt']])])
					nm['comments'] = None
					if 'gnomad_exomes' in data.keys():
						if 'af' in data['gnomad_exomes'][0].keys():
						    nm['af'] = data['gnomad_exomes'][0]['af']
						else:
						    nm['af'] = None
						if 'an' in data['gnomad_exomes'][0].keys():
						    nm['an_tot'] = data['gnomad_exomes'][0]['an']
						else:
						    nm['an_tot'] = None
						if 'ac_afr' in data['gnomad_exomes'][0].keys():
							al_freq['afr'] = {'ac': data['gnomad_exomes'][0]['ac_afr'], 'an': data['gnomad_exomes'][0]['an_afr']}
						if 'ac_amr' in data['gnomad_exomes'][0].keys():
							al_freq['amr'] = {'ac': data['gnomad_exomes'][0]['ac_amr'], 'an': data['gnomad_exomes'][0]['an_amr']}
						if 'ac_eas' in data['gnomad_exomes'][0].keys():
							al_freq['eas'] = {'ac': data['gnomad_exomes'][0]['ac_eas'], 'an': data['gnomad_exomes'][0]['an_eas']}
						if 'ac_nfe' in data['gnomad_exomes'][0].keys():
							al_freq['nfe'] = {'ac': data['gnomad_exomes'][0]['ac_nfe'], 'an': data['gnomad_exomes'][0]['an_nfe']}
						if 'ac_sas' in data['gnomad_exomes'][0].keys():
							al_freq['sas'] = {'ac': data['gnomad_exomes'][0]['ac_sas'], 'an': data['gnomad_exomes'][0]['an_sas']}
						al_freq = pd.DataFrame.from_dict(al_freq, orient='index')
						if 'ac' in al_freq.keys():
							al_freq['af'] = al_freq['ac']/al_freq['an']
						if 'af' in al_freq.keys():
							#nm['max_maf'] = al_freq['af'].max()
							max_maf = al_freq['af'].max()
							nm['an'] = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,0][0]
							nm['ac'] = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,1][0]
							al_freq = al_freq[al_freq.an>=2000]
							nm['max_maf_ba1'] = al_freq['af'].max()
							if max_maf!=nm['max_maf_ba1']:
								nm['comments'] = 'Not taken the maximum pop MAF because it did not have at least 2000 alleles'
						else:
							#nm['max_maf'] = None
							nm['ac'] = None
							nm['an'] = None
							nm['max_maf_ba1'] = None
					else:
						#nm['max_maf'] = None
						nm['af'] = None
						nm['ac'] = None
						nm['an'] = None
						nm['an_tot'] = None
						nm['max_maf_ba1'] = None
					if 'gnomad_genomes' in data.keys():
						if 'af' in data['gnomad_genomes'][0].keys():
							nm['g_af'] = data['gnomad_genomes'][0]['af']
						else:
							nm['g_af'] = None
					else:
						nm['g_af'] = None #['E310.2020', '10380150781237070002', 'chr15:78123707-78123707:G/C', 'rs900609049', 'CIB2:NM_006383:EX2', 'synonymous', 'Uncertain Significance']
					#nm['g_af'] = None
					#nm['saphetor_verdict'] = None
					if 'saphetor_known_pathogenicity' in data.keys():
						#nm['saphetor_verdict'] = data['saphetor_known_pathogenicity']
						if 'Saphetor PubMedUserEntry' in data['saphetor_known_pathogenicity'][0]['items'][0]['annotations'].keys():
							nm['saphetor_user_verdict'] = []
							nm['saphetor_user_pub'] = []
							nm['saphetor_user_classification'] = []
							#nm['saphetor_verdict'] = data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']['Saphetor PubMedUserEntry']#[0]['acmg_class']
							for l in data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']['Saphetor PubMedUserEntry']:
								nm['saphetor_user_verdict'].append(l['acmg_class'])
								nm['saphetor_user_pub'].append(l['pub_med_references'])
						else:
							nm['saphetor_user_verdict'] = None
							nm['saphetor_user_pub'] = None
						if 'Saphetor VarSome Comment' in data['saphetor_known_pathogenicity'][0]['items'][0]['annotations'].keys():
							nm['saphetor_user_classification'] = []
							for l in data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']['Saphetor VarSome Comment']:
								nm['saphetor_user_classification'].append(l['acmg_class'])
						else:
							nm['saphetor_user_classification'] = None
					else:
						nm['saphetor_user_verdict'] = None
						nm['saphetor_user_pub'] = None
						nm['saphetor_user_classification'] = None
					if data['ref']=='':
						data['ref']='-'
					if data['alt']=='':
						data['alt']='-'
					#creo l'hgvs: chr:start-end:ref/alt dove start è la posizione della variante e end è la posizione se la lunghezza di alt è 1 altrimenti è pos+len(alt)
					if len(data['alt'])==1:
						nm['hgvs'] = ':'.join([data['chromosome'], '-'.join([str(data['pos']), str(data['pos'])]), '/'.join([data['ref'], data['alt']])])
					else:
						end = str(data['pos'] + len(data['alt']))
						nm['hgvs'] = ':'.join([data['chromosome'], '-'.join([str(data['pos']), end]), '/'.join([data['ref'], data['alt']])])
					if 'ncbi_dbsnp' in data.keys():
						nm['rsid'] = ''.join(['rs',str(data['ncbi_dbsnp'][0]['rsid'][0])])
					else:
						nm['rsid'] = ''
					if 'ncbi_clinvar2' in data.keys():
						nm['clinvar2'] = True
						nm['clinical_significance'] = None
						if data['ncbi_clinvar2'][0]['review_stars']>0:
							nm['clinvar2_stars'] = data['ncbi_clinvar2'][0]['review_stars']
							nm['clinvar_clinical_significance'] = data['ncbi_clinvar2'][0]['clinical_significance'][0]
							nm['clinvar_acmg_verdict'] = (nm['verdict']==nm['clinvar_clinical_significance'])
						else:
							nm['clinvar2_stars'] = 'less than 1 star'
							nm['clinvar_clinical_significance'] = None
							nm['clinvar_acmg_verdict'] = None
					else:
						nm['clinvar2_stars'] = None
						nm['clinvar2'] = False
						nm['clinvar_clinical_significance'] = None
						nm['clinvar_acmg_verdict'] = None

					for t in data['refseq_transcripts'][0]['items']:
						#il coding impact, se c.i. è nullo leggi function
						nm[t['name'][:-2]]= t['coding_impact']
						#nm['splice_distance'] = t['splice_distance']
						if not nm[t['name'][:-2]]:
							nm[t['name'][:-2]] = t['function'][0]
						if refseq == t['name'][:-2]:
							today = str(datetime.date.today())
							res[i] = [nm['chr'], nm['pos'], nm['hgvs'], today, nm['varsomeid'], nm['rsid'], info, nm[t['name'][:-2]], nm['verdict'], nm['af'], nm['an_tot'], nm['ac'], nm['an'], nm['max_maf_ba1'], nm['g_af'], nm['saphetor_user_verdict'], nm['saphetor_user_pub'], nm['saphetor_user_classification'], nm['clinvar2'], nm['clinvar2_stars'], nm['clinvar_clinical_significance'], nm['clinvar_acmg_verdict'], nm['comments']]#, nm['max_maf']
							i=i+1
				else:
					print('Error: no answere frome Varsome')
		else:
			print('Error: no answere from Varsome region')
			res={}

	res = pd.DataFrame.from_dict(res, orient='index')

	col=['chromosome', 'position', 'hgvs', 'date', 'varsomeid', 'rsid', 'info', 'coding_impact', 'verdict', 'gnomad_exomes_allele_freq', 'gnomad_exomes_allele_number', 'gnomad_exomes_pop_allele_count', 'gnomad_exomes_pop_allele_number', 'exome_max_maf_BA1', 'gnomad_genomes_allele_freq', 'saphetor_user_verdict', 'saphetor_user_pubblications', 'saphetor_user_classification', 'clinvar', 'clinvar_stars', 'clinvar_clinical_significance', 'clinvar_acmg_verdict', 'comments']#, 'exome_max_maf'
	res.sort_values(by=[0, 1], ascending=[True, True])
	res.to_csv(''.join([gene_varsome, GENE, '.csv']), mode='a', index = False, header = col, sep='\t')
	res = pd.read_csv(''.join([gene_varsome, GENE, '.csv']), sep='\t',comment='#')
	res.groupby(["coding_impact", "verdict"])["varsomeid"].count().to_csv(''.join([gene_varsome,GENE, '_clinical_statistics.csv']), index=True, sep='\t', header=False, mode='w')
	return res

def get_variants_info(SAMPLE, path, GENE):
	#print('Function get_variants_info')
	today = datetime.date.today()
	sample = SAMPLE.iloc[0,0]
	t = 200
	if os.path.isfile(''.join([gene_varsome,GENE, '.csv'])):
		date = pd.read_csv(''.join([gene_varsome,GENE, '.csv']), nrows=1, sep='\t').iloc[0,3]
		date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
		if (today-date).days>t: #check if data about gene have more than 60 days
			os.rename(r''+gene_varsome+GENE+'.csv',r''+gene_varsome+'OLD/'+GENE+'_'+str(today)+'.csv')#sposta file gene
			query_varsome_variants_in_gene(SAMPLE, path, GENE)
	else:
		query_varsome_variants_in_gene(SAMPLE, path, GENE)

def varsome_consequences(GENE, SAMPLE, index):
	#print('Function varsome_consequences')
	try:
		gene_res = pd.read_csv(''.join([gene_varsome,GENE, '.csv']), header=0, sep='\t')
		gene_stats = pd.read_csv(''.join([gene_varsome,GENE, '_clinical_statistics.csv']), header=None, sep='\t')
		SAMPLE = BP1(gene_stats, SAMPLE, index)
		SAMPLE = BS1(gene_res, SAMPLE, index)
		SAMPLE = BA1(gene_res, SAMPLE, index)
		#SAMPLE.loc[index, 'gene_maf'] =  pd.to_numeric(gene_res.exome_max_maf_BA1).max()
		# SAMPLE.loc[index, 'definitivemaf2'] = np.where(SAMPLE.loc[index,'BA1'] == None, SAMPLE.loc[index,'decisionmaf'], SAMPLE.loc[index,'BA1'] )
		# SAMPLE.loc[index, 'definitivemaf2'] = np.where(SAMPLE.loc[index,'definitivemaf2'] == -999, SAMPLE.loc[index,'definitivemaf'], SAMPLE.loc[index,'definitivemaf2'])
	except:
		print('Files about gene '+ GENE + ' not found')
		if str(SAMPLE.loc[index, 'comments']) != 'nan':
			SAMPLE.loc[index, 'comments'] = SAMPLE.loc[index, 'comments'] + ', ' + ('Files about gene '+ GENE + ' not found')
		else:
			SAMPLE.loc[index, 'comments'] = ('Files about gene '+ GENE + ' not found')
		SAMPLE.loc[index,'BA1'] = None
		SAMPLE.loc[index,'pop_allele_count'] = None
		SAMPLE.loc[index,'BP1_1'] = -999
		SAMPLE.loc[index,'BP1_2'] = -999
		SAMPLE.loc[index,'BP1_3'] = -999
		SAMPLE.loc[index,'BS1'] = 0.0005
		# SAMPLE.loc[index, 'definitivemaf2'] = SAMPLE.loc[index,'decisionmaf']
		# SAMPLE.loc[index, 'definitivemaf2'] = np.where(SAMPLE.loc[index,'definitivemaf2'] == -999, SAMPLE.loc[index,'definitivemaf'], SAMPLE.loc[index,'definitivemaf2'])
	return SAMPLE

def BP1(gene_stats, SAMPLE, index):
	mis = gene_stats[gene_stats.iloc[:,0] == 'missense']
	#1) almeno 30 varianti missenso NON-VUS riportate nel trascritto
	bp1_1 = mis[(mis.iloc[:,1] != 'Uncertain Significance')].iloc[:,2].sum()
	#2) [n° missenso (B+VB)/n° missenso NON-VUS>=90%]
	mis_b = mis[(mis.iloc[:,1] == 'Benign') | (mis.iloc[:,1] == 'Likely Benign')].iloc[:,2].sum()
	mis_n_v = mis_b + mis[(mis.iloc[:,1] == 'Pathogenic') | (mis.iloc[:,1] == 'Likely Pathogenic')].iloc[:,2].sum()
	bp1_2 = mis_b/mis_n_v
	#3) [n° (B+VB)/totale varianti]>=15%
	tot_b = gene_stats[(gene_stats.iloc[:,1] == 'Benign') | (gene_stats.iloc[:,1] == 'Likely Benign')].iloc[:,2].sum()
	bp1_3 = tot_b/(gene_stats.iloc[:,2].sum())
	SAMPLE.loc[index,'BP1_1'] = bp1_1
	SAMPLE.loc[index,'BP1_2'] = bp1_2
	SAMPLE.loc[index,'BP1_3'] = bp1_3
	return SAMPLE

def BS1(gene_res, SAMPLE, index):
	#max MAF tra lof pat ES: manca da mettere le splicing junction loss perchè c'è da capire come vedere solo quelle in siti canonici
	mis = gene_res[((gene_res.coding_impact == 'missense') | (gene_res.coding_impact == 'frameshift') | (gene_res.coding_impact == 'nonsense') | (gene_res.coding_impact == 'start loss') | (gene_res.coding_impact == 'stopLoss')) & (gene_res.verdict == 'Pathogenic')] #((gene_res.verdict == 'Likely Pathogenic') | (gene_res.verdict == 'Pathogenic'))]
	maf = pd.to_numeric(mis.exome_max_maf_BA1)
	bs1 = maf.max()
	SAMPLE.loc[index,'BS1'] = bs1
	return SAMPLE

def BA1(gene_res, SAMPLE, index):
	var = gene_res[gene_res.hgvs == SAMPLE.loc[index, 'HGVS']]
	maf =  var.exome_max_maf_BA1 #gnomad_exomes_pop_allele_count
	if len(maf)>0:
		SAMPLE.loc[index,'BA1'] = float(maf.iloc[0])
		SAMPLE.loc[index,'pop_allele_count'] = var.gnomad_exomes_pop_allele_count.iloc[0]
	else:
		SAMPLE.loc[index,'BA1'] = None
		SAMPLE.loc[index,'pop_allele_count'] = None
	return SAMPLE

def make_intron(SAMPLE):
	xxx = SAMPLE[SAMPLE['consequence'].str.contains('intron')]
	xxx.loc[:,'ada_score'] = xxx['ada_score'].astype(float)
	xxx.loc[:,'rf_score'] = xxx['rf_score'].astype(float)
	xxx.loc[:,'predictors_introns'] = np.where((xxx['rf_score'].isnull()) | (xxx['ada_score'].isnull()), 'NULL', np.where((xxx['rf_score'] >= 0.6) & (xxx['ada_score'] >= 0.6),'D','B'))
	SAMPLE2 = pd.merge(SAMPLE,xxx[['CHROM','POS','predictors_introns']],on=['CHROM','POS'],how='left')
	return SAMPLE2

def find_benignity(SAMPLE):
	benign1=((SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['definitivemaf'] > float(0.014)))
	SAMPLE = SAMPLE[~benign1]
	benign2=((SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['controls_nhomalt'] > int(3)))
	SAMPLE = SAMPLE[~benign2]
	benign1AD=((SAMPLE['inheritance']=='AD') & (SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['definitivemaf'] > float(0.0001)))
	SAMPLE = SAMPLE[~benign1AD]
	benign2AD=((SAMPLE['inheritance']=='AD') & (SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['controls_nhomalt'] > int(12)))
	SAMPLE = SAMPLE[~benign2AD]
	benign1int=((SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['definitivemaf'] > float(0.014)))
	SAMPLE = SAMPLE[~benign1int]
	benign2int=((SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['controls_nhomalt'] > int(3)))
	SAMPLE = SAMPLE[~benign2int]
	benign1intAD=((SAMPLE['inheritance']=='AD') & (SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['definitivemaf'] > float(0.0001)))
	SAMPLE = SAMPLE[~benign1intAD]
	benign2intAD=((SAMPLE['inheritance']=='AD') & (SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['controls_nhomalt'] > int(12)))
	SAMPLE = SAMPLE[~benign2intAD]
	synonimous=((SAMPLE['consequence']=='synonymous_variant') & (SAMPLE['comments'].isnull()))
	SAMPLE = SAMPLE[~synonimous]
	fromACMG=((SAMPLE['acmg_verdict'] =='Benign') & ((SAMPLE['predictors_decision'].astype(str)=='B') | (SAMPLE['controls_nhomalt'] > int(3))) & (SAMPLE['predictors_decision'].astype(str)!='D') & (SAMPLE['Description'].astype(str)!='SELECTED'))
	SAMPLE = SAMPLE[~fromACMG]
	fromFILE=((SAMPLE['fromFILE']==1))
	SAMPLE = SAMPLE[fromFILE]
	return SAMPLE

def find_benignity2(SAMPLE):
	#print('find_benignity2')
	SAMPLE['definitivemaf2'].fillna(-999,inplace=True)
	ba1 = (SAMPLE['BA1']>=0.05)
	ba1 = ba1.rename("BA1")
	# BS1:  2 valori, uno per quello basato sulla prevalenza (solo quelli selezionati), l'altro per quello basato sulla max MAF di gnomAD ----- #+2
	bs1_maf = (SAMPLE['definitivemaf2'] >= SAMPLE['BS1'])
	bs1_maf = bs1_maf.rename("BS1_maf")
	bs1_prev = (SAMPLE['definitivemaf2'] >= SAMPLE['prevalence'])
	bs1_prev = bs1_prev.rename("BS1_prev")
	benign2=(SAMPLE['controls_nhomalt'] > int(3)) #+1 BS2
	benign2 = benign2.rename("benign2")
	benign2AD=((SAMPLE['inheritance']=='AD') & (SAMPLE['controls_nhomalt'] > int(5))) #+1 BS2
	benign2AD = benign2AD.rename("benign2AD")
	bp1 = (SAMPLE['BP1_1']>=30) & (SAMPLE['BP1_2']>=0.90) & (SAMPLE['BP1_3']>=0.15)
	bp1 = bp1.rename("BP1")
	# BP4 -------- #+1
	benign1= ((SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['predictors_decision'].astype(str)!='NULL')) #+1 BP4
	benign1 = benign1.rename("benign1")
	notbenign = ((SAMPLE['predictors_decision'].astype(str)=='D') & (SAMPLE['predictors_decision'].astype(str)!='NULL')) # punteggio -1 PP3,
	notbenign = notbenign.rename("notbenign")
	benign2int = ((SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['predictors_introns'].astype(str)!='NULL')) #+1 BP4
	benign2int = benign2int.rename("benign2int")
	notbenignint = ((SAMPLE['predictors_introns'].astype(str)=='D') & (SAMPLE['predictors_introns'].astype(str)!='NULL')) #-1 BP4
	notbenignint = notbenignint.rename("notbenignint")
	# BP6 -------- #+1
	fromACMG=(SAMPLE['acmg_verdict'] =='Benign') #+ 1 BP6
	fromACMG = fromACMG.rename("fromACMG")
	#synonimous=((SAMPLE['consequence']=='synonymous_variant') & (SAMPLE['comments'].isnull()))
	#synonimous = synonimous.rename("synonymous")
	fromFILE=((SAMPLE['fromFILE']==1))
	fromFILE = fromFILE.rename("fromFILE")
	checklist_bool = pd.concat([bs1_prev, ba1, bs1_maf, benign2, benign2AD, bp1, benign1, notbenign, benign2int, notbenignint, fromACMG], axis=1)

	checklist_val = checklist_bool.copy()
	checklist_val2 = checklist_bool.copy()

	checklist_val['BA1_points'] = np.where(checklist_val['BA1'] == True, 3, 0)
	checklist_val['BS1_maf_points'] = np.where(checklist_val['BS1_maf'] == True, 2, 0)
	checklist_val['benign2_points'] = np.where(checklist_val['benign2'] == True, 1, 0)
	checklist_val['benign2AD_points'] = np.where(checklist_val['benign2AD'] == True, 1, 0)
	checklist_val['BP1_points'] = np.where(checklist_val['BP1'] == True, 1, 0)
	checklist_val['benign1_points'] = np.where(checklist_val['benign1'] == True, 1, 0)
	checklist_val['notbenign_points'] = np.where(checklist_val['notbenign'] == True, -1, 0)
	checklist_val['benign2int_points'] = np.where(checklist_val['benign2int'] == True, 1, 0)
	checklist_val['notbenignint_points'] = np.where(checklist_val['notbenignint'] == True, -1, 0)
	checklist_val['fromACMG_points'] = np.where(checklist_val['fromACMG'] == True, 1, 0)
	checklist_val2['BS1_prev_points'] = np.where(checklist_val['BS1_prev'] == True, 2, 0)
	checklist_val = checklist_val.loc[:,'BA1_points':]
	checklist_val2 = checklist_val2.loc[:,'BS1_prev_points':]
	checklist_val["checklist_points_sum"] = checklist_val.sum(axis=1)
	_checklist_val_final_ = pd.concat([SAMPLE, checklist_val2.loc[:, 'BS1_prev_points':]], axis=1)
	checklist_val_final = pd.concat([_checklist_val_final_, checklist_val.loc[:, 'BA1_points':]], axis=1)
	checklist_val_final = checklist_val_final.drop(['BA1'], axis=1)#, 'gene_maf'
	checklist_val_final = checklist_val_final.rename(index=str, columns={'BP1_1': 'num_mis', 'BP1_2': 'perc_misB', 'BP1_3':'perc_totMIS', 'BS1':'gene_maf'})

	#Selected from exception
	#print (len(checklist_val_final),1)
	checklist_val_final = checklist_val_final[(~(checklist_val_final['acmg_verdict'].str.contains('Benign')) | (checklist_val_final['Description'].astype(str)=='SELECTED'))]
	#print (len(checklist_val_final),2)
	checklist_val_final1 = checklist_val_final[~(checklist_val_final['Description'].str.contains('DISCARDED',na=False))]
	#print (len(checklist_val_final1),3)
	checklist_val_final2 = checklist_val_final1[~(checklist_val_final1['Description'] == 'NOT SELECTED')]
	#print (len(checklist_val_final2),4)
	synonimous = checklist_val_final2[((checklist_val_final2['consequence']=='synonymous_variant') & (checklist_val_final2['acmg_verdict']=='Pathogenic'))]
	checklist_val_final3 = checklist_val_final2[(checklist_val_final2['consequence']!='synonymous_variant')]
	checklist_val_final4 = pd.concat([checklist_val_final3,synonimous])
	#print (len(checklist_val_final4),5)
	intron15 = checklist_val_final2[((checklist_val_final2['unbalance']=='del=nan') & (checklist_val_final2['acmg_verdict']=='Pathogenic'))]
	checklist_val_final4a = checklist_val_final4[(checklist_val_final4['unbalance']!='del=nan')]
	checklist_val_final5 = pd.concat([checklist_val_final4a,intron15])
	#print (checklist_val_final5[checklist_val_final5['HGVS']=='chr1:197328873-197328873:T/A'][['HGVS','definitivemaf2','decisionmaf','decisionINFO','MAF<20']],'11111')
	#print (len(checklist_val_final5),6)
	############################################################
	checklist_val_final6 = checklist_val_final5[checklist_val_final5['definitivemaf2'] <= 0.03]
	#print (checklist_val_final6[checklist_val_final6['HGVS']=='chr1:197328873-197328873:T/A'][['HGVS','definitivemaf2','decisionmaf','decisionINFO','MAF<20']],'11111')
	#print (len(checklist_val_final6),7)
	return checklist_val_final6
#######################################################################################
if __name__=="__main__":
	begin_time = datetime.datetime.now()
	cols1 = ['sample','CHROM','POS','GENE','HGVS','ID','consequence','unbalance','decisionmaf','decisionINFO','definitivemaf',
			'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
			'MAF_selection','predictors_decision','predictors_B','predictors_D','controls_nhomalt','allphenotype',
			'allinheritance','inheritance','prevalence','ada_score','rf_score','HGVS_c']

	args=InputPar()
	#Declaring Variables
	prev='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/Prevalenza.csv'
	prev_maf='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/Freq_maf.csv'

	#try:
	if 'OCULAR' in args.panel: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/omim_OCULARE.csv'
	elif 'VASCULAR' in args.panel:sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'
	elif 'NEUROLOGY' in args.panel: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'
	elif 'MIXED' in args.panel: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/omim_MIXED1.csv'
	elif 'LYMPHOBESITY' in args.panel: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/omim_LYMPHOBESITY.csv'
	elif 'INFERTILITA' in args.panel: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'
	else: sospetti_omim='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'
	#except:
	#sospetti_omim='/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'

	exception='/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/eccezioni.csv'
	regioniproblematiche= '/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/regioniproblematiche.txt'
	gene_varsome = '/home/remo/PROJECT/pacbio/bin/SELEZIONEAUTOMATICA/GENEVAR/'

	#PRED=['CADD','DANN','EigenPC','FATHMM',
	#	'GERP','LRT','MCAP','MetaLR',
	#	'MetaSVM','MutPred','MutationAssessor',
	#	'MutationTaster','PROVEAN','Polyphen2HDIV',
	#	'Polyphen2HVAR','REVEL','SIFT','SiPhy29way',
	#	'VEST3','fathmmMKL','phastCons100way','phastCons20way',
	#	'phyloP100way','phyloP20way']

	PRED=['CADD','DANN','EigenPC','FATHMM',
		'LRT','MCAP','MetaLR',
		'MetaSVM','MutPred','MutationAssessor',
		'MutationTaster','PROVEAN','Polyphen2HDIV',
		'Polyphen2HVAR','REVEL','SIFT',
		'VEST3']

	traduttore={'D':'D','P':'D','T':'B','N':'B','H':'D','B':'B','A':'D','L':'B','M':'D','-999.0':'NULL','-999':'NULL','.':'NULL','U':'NULL'}
	traduttore2={'D':'D','P':'B','T':'B','N':'B','H':'D','B':'B','A':'D','L':'B','M':'D','-999.0':'NULL','-999':'NULL','.':'NULL','U':'NULL'}

	# LOAD REQUIRED FILES
	# Load OMIM file
	SOSPETTI=load_omim(sospetti_omim)

	# Load Prevalence file
	PREV=pd.read_csv(prev, sep=',')
	PREV['Sospetto diagnostico'] = PREV['Sospetto diagnostico'].str.upper()
	PREV.fillna('NA', inplace=True)

	# Load Prevalence-MAF conversion table
	PREV_MAF=pd.read_csv(prev_maf, sep='\t')
	PREV_MAF.fillna('NA', inplace=True)

	folder_name = step.principal_folder(args,step.create_folder(),over=args.over)
	folder_coverage = join(folder_name,'coverage/')
	folder_final = join(folder_name,'final/')
	folder_pheno = join(folder_name,'pheno/')
	lastFOLDER = folder_name.split('/')[-1]
	folder_list = glob.glob(folder_final+'*')

	if args.dest == 'r':
		dest = 'rovereto'
		path_django = '/home/bioinfo/VIRTUAL/MAGIS/NGS_RESULT/annot'
		database = '/home/bioinfo/VIRTUAL/MAGIS/magis.db'
	elif args.dest == 'b':
		dest = 'bolzano'
		path_django = '/home/bioinfo/VIRTUAL/EUREGIO/NGS_RESULT/annot'
		database = '/home/bioinfo/VIRTUAL/EUREGIO/euregio.db'
	elif args.dest == 'z':
		dest = 'ricerca'
		path_django = '/home/bioinfo/VIRTUAL/RICERCA/NGS_RESULT/annot'
		database = '/home/bioinfo/VIRTUAL/RICERCA/ricerca.db'
	#################################################
	SAMPLE_DS = pd.read_csv(join(folder_pheno,'phenotype'),sep='\t',header=0)
	SAMPLE_to_analyze=sorted(glob.glob(join(folder_final,'*.longshot.filt.appris.vcf')))
	#predictors=sorted(glob.glob(join(folder_final,'*_other_annot.csv')))
	##################################################
    col_annot =
	for folder_sample in set(SAMPLE_to_analyze):
			sample_x = folder_sample.split('/')[-1]
			sample_x = str(sample_x)
			print ('----->'+sample_x.split('_')[0]+'<------')
			col_annot = ['sample', 'HGVS', 'CHROM', 'POS', 'GENE', 'ID', 'CADD_rankscore', 'DANN_rankscore', 'EigenPC_rankscore', 'FATHMM_rankscore', 'FATHMM_pred', 'GERP_rankscore', 'Interpro_domain', 'LRT_rankscore', 'LRT_pred', 'MCAP_pred', 'MetaLR_pred', 'MetaLR_rankscore', 'MetaSVM_pred', 'MetaSVM_rankscore', 'MutPred_rankscore', 'MutationAssessor_pred',
            'MutationAssessor_rankscore', 'MutationTaster_rankscore', 'MutationTaster_pred', 'PROVEAN_rankscore', 'PROVEAN_pred', 'Polyphen2HDIV_pred', 'Polyphen2HDIV_rankscore', 'Polyphen2HVAR_pred', 'Polyphen2HVAR_rankscore', 'REVEL_rankscore', 'SIFT_rankscore', 'SIFT_pred', 'SiPhy29way_rankscore', 'VEST3_rankscore', 'clinvar_clnsig', 'fathmmMKL_pred',
            'phastCons100way_rankscore', 'phastCons20way_rankscore', 'phyloP100way_rankscore', 'phyloP20way_rankscore', 'ada_score', 'rf_score']
			if True:
			#if (sample_x.split('_')[0]=='E378.2020'):
				#if (sample_x.split('_')[0]=='E472.2020'):  #&(sample_x.split('_')[0] != 'E416.2020'):
					temp0=pd.read_csv(folder_sample, dtype=str, sep='\t')
					temp0.fillna('-999',inplace=True)
					#temp1=pd.read_csv(join(folder_final,sample_x.split('_')[0]+'_other_annot.csv'), dtype=str, sep='\t')
                    temp1 = pd.DataFrame(columns = col_annot)
					##########################################################
					genes = SAMPLE_DS[SAMPLE_DS['sample']==sample_x.split('_')[0]]['gene'].values #.str.translate()
					ds = SAMPLE_DS[SAMPLE_DS['sample']==sample_x.split('_')[0]]['malattia'].values
					# load sample files and create a unique dataframe
					SAMPLE=load_SAMPLE_data(temp0,temp1)
					SAMPLE['MAF<20'] = np.NaN
					# FILTERS
					# Invert variants with MAF > 0.8
					print (len(SAMPLE))
					SAMPLE=invert_MAF(SAMPLE,cols1)
					# Include variants from exception file
					SAMPLE=include_exceptions(SAMPLE, exception)
					# Include variants from regioni problematiche
					SAMPLE=exclude_problemregion(SAMPLE,regioniproblematiche)
					# Exclude variants with MAF > 0.3
					SAMPLE=cut_MAF(SAMPLE,cols1)
					# Exclude variants above MAF treshold
					SAMPLE=MAF_t(SAMPLE,SOSPETTI,genes,ds[0],PREV_MAF,PREV,cols1)
					# PREDICTORS
					# assign predictions to variants
					SAMPLE=make_predictions(SAMPLE, traduttore,traduttore2,PRED,cols1)
					sample_ = sample_x.split('_')[0]
					PREresult1 = join(folder_final,sample_+'_PREvariant_selection.csv')
					result1 = join(folder_final,sample_+'_variant_selection.csv')
					result_django1 = join(path_django,sample_+'_variant_selection.csv')
					try: SAMPLE['definitivemaf'] = SAMPLE['decisionmaf']
					except: SAMPLE['definitivemaf'] = None
					#####################################################
					#xxx = SAMPLE[cols1][SAMPLE['unbalance']!='del=nan']	#
					xxx = SAMPLE[cols1]
					#####################################################
					print (len(xxx))
					for index,row in xxx.iterrows():
							rawinput = (row.HGVS_c).split(':')[0].split('.')[0]+':'+(row.HGVS_c).split(':')[1]
							rawinput2 = row.GENE+':'+(row.HGVS_c).split(':')[1]
							#try:
								#print (rawinput)
							saphetor = query_varsome_saphetor(rawinput, rawinput2 ,xxx,index)
							#print(saphetor[['definitivemaf2', 'decisionmaf', 'decisionINFO']])
							saphetor = query_varsome_gene(str(row.GENE),saphetor,index)#str(row.GENE)
							saphetor = varsome_consequences(str(row.GENE), saphetor, index)#str(row.GENE)

							#except:
					 		#print('except')
								#metti a nulli i valori riempiti prima?
								# saphetor=query_varsome_saphetor(rawinput2,xxx,index)
								# saphetor = query_varsome_gene(str(row.GENE),saphetor,index)#str(row.GENE)
								# #get_variants_info(saphetor, folder_pheno, str(row.GENE))
								# saphetor = varsome_consequences(str(row.GENE), saphetor, index)
			##############################################################################
					if len(xxx) == 0:
						# SAMPLE = pd.DataFrame(columns=['sample','HGVS','CHROM','POS','ID','GENE','consequence','HGVS_c','inheritance','decisionmaf',
						# 		'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
						# 		'MAF_selection','controls_nhomalt','fromFILE',
						# 		'predictors_decision','predictors_B','predictors_D','predictors_introns',
						# 		'acmg_verdict','acmg_classificatins','publications','clinvar2','saphetor','mis_z', 'prevalence', 'gene_maf', 'definitivemaf2'])
						SAMPLE = pd.DataFrame(columns=['sample_id','HGVS','CHROM','POS','ID','GENE','consequence','HGVS_c','unbalance',
									'inheritance','allphenotype','allinheritance','definitivemaf','definitivemaf2','pop_allele_count',
									'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
									'MAF_selection', 'predictors_decision','predictors_B','predictors_D','predictors_introns',
									'controls_nhomalt','prevalence','ada_score','rf_score',
									'acmg_verdict','acmg_classificatins',
									'publications','clinvar2','saphetor','mis_z','lof_z','num_mis',
									'perc_misB','perc_totMIS','gene_maf',
									'fromFILE','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
									'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
									'benign2int_points','notbenignint_points','fromACMG_points','checklist_points_sum'])
					else:
						SAMPLE = saphetor
						#print(SAMPLE[['definitivemaf2', 'decisionmaf', 'decisionINFO']])
						#decisionINFO = SAMPLE[['HGVS', 'decisionINFO']]
						# PREDICT INTRON FUNCIONAL
						SAMPLE = make_intron(SAMPLE)
						SAMPLE=exclude_problemregion(SAMPLE,regioniproblematiche)
						#SAMPLE = SAMPLE[SAMPLE['Description']=='SELECTED'] #to be commented
						SAMPLE.to_csv(PREresult1,sep='\t',index=False)

						SAMPLE['definitivemaf2'].fillna(-999,inplace=True)
						SAMPLE['decisionINFO'].fillna(-999,inplace=True)


						tempF=pd.read_csv(folder_sample, dtype=str, sep='\t')
						decisional = pd.merge(tempF[['HGVS','decisionmaf','decisionINFO']],SAMPLE[['HGVS','definitivemaf2', 'decisionINFO']],on='HGVS',how='left')
						#print('decisional')
						#print(tempF[['HGVS','decisionmaf']])
						#print(SAMPLE[['HGVS','definitivemaf2']])
						decisional.to_csv('decisional.csv')
						#print(decisional[['definitivemaf2', 'decisionmaf', 'decisionINFO']])
						decisional['definitivemaf2'].fillna(decisional['decisionmaf'],inplace=True)
						decisional['decisionINFO_y'].fillna(decisional['decisionINFO_x'],inplace=True)
						tempF['decisionmaf'] = decisional['definitivemaf2']
						#tempF['decisionINFO'] = np.where(tempF['decisionINFO']!=-999,tempF['decisionINFO'],'VARSOME')
						tempF['decisionINFO'] = decisional['decisionINFO_y']
						#print(tempF)
						tempF.to_csv((join(folder_final,sample_x.split('_')[0]+'_pheno_predict.csv')),sep='\t',index=False)
						tempF.to_csv((join(path_django,sample_x.split('_')[0]+'_pheno_predict.csv')),sep='\t',index=False)


						SAMPLE = find_benignity2(SAMPLE)
						SAMPLE['sample_id'] = SAMPLE['sample']
						SAMPLE = SAMPLE[['sample_id','HGVS','CHROM','POS','ID','GENE','consequence','HGVS_c','unbalance',
							'inheritance','allphenotype','allinheritance','definitivemaf','definitivemaf2','pop_allele_count',
							'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
							'MAF_selection', 'predictors_decision','predictors_B','predictors_D','predictors_introns',
							'controls_nhomalt','prevalence','ada_score','rf_score',
							'acmg_verdict','acmg_classificatins',
							'publications','clinvar2','saphetor','mis_z','lof_z','num_mis',
							'perc_misB','perc_totMIS','gene_maf',
							'fromFILE','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
							'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
							'benign2int_points','notbenignint_points','fromACMG_points','checklist_points_sum']]

					SAMPLE['definitivemaf'].fillna(-999,inplace=True)
					SAMPLE['definitivemaf2'].fillna(-999,inplace=True)
					SAMPLE['ada_score'].fillna(-999,inplace=True)
					SAMPLE['rf_score'].fillna(-999,inplace=True)
					SAMPLE['pop_allele_count'].fillna(0,inplace=True)
					SAMPLE['lof_z'].fillna(0,inplace=True)
					SAMPLE['num_mis'].fillna(0,inplace=True)
					SAMPLE['perc_misB'].fillna(-999,inplace=True)
					SAMPLE['perc_totMIS'].fillna(-999,inplace=True)
					SAMPLE['gene_maf'].fillna(-999,inplace=True)
					SAMPLE['MAF<20'] = SAMPLE['MAF<20'].astype(int)
					SAMPLE['pop_allele_count'] = SAMPLE['pop_allele_count'].astype(int)
					SAMPLE['lof_z'] = SAMPLE['lof_z'].astype(int)
					SAMPLE['num_mis'] = SAMPLE['num_mis'].astype(int)
					SAMPLE['ada_score'].fillna(-999,inplace=True)
					SAMPLE['rf_score'].fillna(-999,inplace=True)
					SAMPLE['prevalence'].fillna(0.022,inplace=True)
					SAMPLE['allinheritance'] = [set(x.strip('[]()\'').strip('None').strip('Unkwnow').split(',')) for x in SAMPLE['allinheritance']]
					SAMPLE['allphenotype'] = [set(x.strip('[]()').strip('None').strip('Unkwnow').split('\' \'')) for x in SAMPLE['allphenotype']]
					SAMPLE['controls_nhomalt'] = SAMPLE['controls_nhomalt'].astype(int)
					SAMPLE['mis_z'].fillna(-999,inplace=True)
					SAMPLE['mis_z'] =  SAMPLE['mis_z'].map('{:,.5f}'.format)
					SAMPLE.to_csv(result1,sep='\t',index=False)
					SAMPLE.to_csv(result_django1,sep='\t',index=False)
					#system(' '.join(['scp',result_django1, "remo@192.168.4.230:/home/remo/venv/apimagi_dev/NGS_RESULT/annot/"]))
					#system(' '.join(['scp',result_django1, "remo@192.168.4.230:/home/remo/venv/apimagi_prod/NGS_RESULT/annot/"]))
					# tempF=pd.read_csv(folder_sample, dtype=str, sep='\t')
					# decisional = pd.merge(tempF[['HGVS','decisionmaf']],SAMPLE[['HGVS','definitivemaf2']],on='HGVS',how='left')
					# print('decisional')
					# print(tempF[['HGVS','decisionmaf']])
					# print(SAMPLE[['HGVS','definitivemaf2']])
					# decisional.to_csv('decisional.csv')
					# print(decisional[['definitivemaf2', 'decisionmaf', 'decisionINFO']])
					# # decisional['definitivemaf2'].fillna(decisional['decisionmaf'],inplace=True)
					# tempF['decisionmaf'] = decisional['definitivemaf2']
					# #tempF['decisionINFO'] = np.where(tempF['decisionINFO']!=-999,tempF['decisionINFO'],'VARSOME')
					# tempF['decisionINFO'] = decisional['decisionINFO']
					# #print(tempF)
					# tempF.to_csv((join(folder_final,sample_x.split('_')[0]+'_pheno_predict.csv')),sep='\t',index=False)
					# tempF.to_csv((join(path_django,sample_x.split('_')[0]+'_pheno_predict.csv')),sep='\t',index=False)
					#system(' '.join(['scp',(join(folder_final,sample_x.split('_')[0]+'_pheno_predict.csv')), "remo@192.168.4.230:/home/remo/venv/apimagi_dev/NGS_RESULT/annot/"]))
					#system(' '.join(['scp',(join(folder_final,sample_x.split('_')[0]+'_pheno_predict.csv')), "remo@192.168.4.230:/home/remo/venv/apimagi_prod/NGS_RESULT/annot/"]))
