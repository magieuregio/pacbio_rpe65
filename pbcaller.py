#!/usr/bin/python3

#importing Python required modules
import argparse
import subprocess
import datetime
import json
import glob
import gzip
import time
import sys
import os
import re

#current directory where this script is placed
dirpath = os.path.dirname(os.path.abspath(__file__))

class Analysis(object):
	""" Detects sequencing data from the input directory """
	# def getFiles(self, path):
	# 	""" This function reads the input directory and identifies FASTQ data """
	# 	extensions = (
	# 		path+"/*.subreads.fq",
	# 		path+"/*.subreads.fastq",
	# 		path+"/*.subreads.fq.gz",
	# 		path+"/*.subreads.fastq.gz"
	# 	)
	# 	files = [] #all files with the extensions above will be stored in this list
	# 	for extension in extensions:
	# 		files.extend(glob.glob(extension))
	# 	return files

	# def getSamples(self, files):
	# 	""" This function checks filenames in order to identify samples. The idea here is that
	# 	some samples have multiple subread files. """
	# 	samples = {} #all samples with the corresponding sequencing files will be stored in this dictionary
	# 	for file_ in files:
	# 		sample = os.path.basename(file_).split(".")
	# 		if sample[-1] == "gz": #input files are compressed
	# 			idx = 4
	# 		else:
	# 			idx = 3
	# 		#a sample might be split into different FASTQ files. In this case, filenames will be
	# 		#name.1.fastq, name.2.fastq and so on. We check here if this is the case.
	# 		if sample[-idx].isdigit():
	# 			sample = ".".join(sample[0:(len(sample)-idx)])
	# 		else:
	# 			sample = ".".join(sample[0:(len(sample)-idx+1)])
	# 		#filling dictionary
	# 		if sample not in samples:
	# 			samples[sample] = [file_]
	# 		else:
	# 			samples[sample].append(file_)
	# 	return samples

	def is_blank(self, s):
		""" This function tests whether a string is blank/empty """
		return not bool(s and s.strip())

	def readConfig(self, path, reference, bed):
		""" This function reads the input configuration file """
		samples = {} #all samples with the corresponding sequencing files will be stored in this dictionary
		try:
			f = open(path+"/config.json", "r") #opening JSON file
			contents = json.load(f) #reading JSON contents
			f.close() #closing JSON file
		except:
			sys.stderr.write("\n[ERROR] The configuration file does not exist or has has a malformed JSON format.\n\n")
			sys.exit(1)
		# n = 1 #line counter
		# for line in f:
		# 	sline = line.rstrip("\n").split("\t") #removing line breaks and splitting row by tabs
		# 	if n == 1 and self.is_blank(sline[0]): #1st row, 1st cell should contain a sample name
		# 		sys.stderr.write("\n[ERROR] Sample name was not set in the first row of the configuration file.\n\n")
		# 		sys.exit(1)
		# 	if not self.is_blank(sline[0]): #sample name found in the 1st column
		# 		sample = sline[0]
		# 		if sample not in samples:
		# 			samples[sample] = []
		# 	if not self.is_blank(sline[1]):
		# 		if not os.path.isfile(path+"/"+sline[1]):
		# 			sys.stderr.write("\n[ERROR] The file '"+sline[1]+"' was not found in the input directory.\n\n")
		# 			sys.exit(1)
		# 		samples[sample].append(path+"/"+sline[1])
		# 	n += 1
		# f.close()
		if "samples" not in contents or "genome" not in contents or "VEPspecies" not in contents or "VEPassembly" not in contents: #checking the structure of the configuration file
			sys.stderr.write("\n[ERROR] The configuration file seems to have a wrong format. Either 'samples', 'genome', 'VEPspecies' or 'VEPassembly' key is missing.\n\n")
			sys.exit(1)
		if not os.path.isfile(reference+"/"+contents["genome"]): #checking whether the reference genome exists
			sys.stderr.write("\n[ERROR] The reference file '"+reference+"/"+contents["genome"]+"' does not exist.\n\n")
			sys.exit(1)
		if contents["genome"].endswith(".gz"): #checking whether the reference genome is compressed
			sys.stderr.write("\n[ERROR] The reference genome is compressed. It needs to be uncompressed. You can use the following command: 'gunzip "+reference+"/"+contents["genome"]+"' to uncompress it. Once uncompressed, please remember to change the configuration file.\n\n")
			sys.exit(1)
		if not contents["genome"].endswith(".fa") and not contents["genome"].endswith(".fasta"): #only extensions .fa/.fasta allowed for the reference genome
			sys.stderr.write("\n[ERROR] Only .fa/.fasta extensions are allowed for the reference genome. The following file has none of the mentioned extensions: '"+reference+"/"+contents["genome"]+"'.\n\n")
			sys.exit(1)
		contents["genome"] = reference+"/"+contents["genome"] #path to the directory containing the genome / genome file name
		if not os.path.isfile(bed+"/"+contents["target"]): #checking whether the target file exists
			sys.stderr.write("\n[ERROR] The target file '"+bed+"/"+contents["target"]+"' does not exist.\n\n")
			sys.exit(1)
		if not contents["target"].endswith(".bed"): #checking whether the target file has the extension BED
			sys.stderr.write("\n[ERROR] Only .bed extension is allowed for the target file.\n\n")
			sys.exit(1)
		contents["target"] = bed+"/"+contents["target"] #path to the directory containing the target file / target file name
		if "VEPdbNSFP" not in contents:
			sys.stderr.write("[WARNING] The configuration file does not contain the key 'VEPdbNSFP'. The annotation will be performed with Ensembl-VEP without using this plugin.\n")
			contents["VEPdbNSFP"] = None
		elif not os.path.isfile(reference+"/"+contents["VEPdbNSFP"]): #checking whether dbNSFP file exists:
			sys.stderr.write("\n[ERROR] The dbNSFP (Ensembl-VEP plugin) file '"+reference+"/"+contents["VEPdbNSFP"]+"' does not exist.\n\n")
			sys.exit(1)
		else:
			contents["VEPdbNSFP"] = reference+"/"+contents["VEPdbNSFP"]
		if "VEPdbscSNV" not in contents:
			sys.stderr.write("[WARNING] The configuration file does not contain the key 'VEPdbscSNV'. The annotation will be performed with Ensembl-VEP without using this database.\n")
			contents["VEPdbscSNV"] = None
		elif not os.path.isfile(reference+"/"+contents["VEPdbscSNV"]): #checking whether dbscSNV file exists:
			sys.stderr.write("\n[ERROR] The dbscSNV (Ensembl-VEP plugin) file '"+reference+"/"+contents["VEPdbscSNV"]+"' does not exist.\n\n")
			sys.exit(1)
		else:
			contents["VEPdbscSNV"] = reference+"/"+contents["VEPdbscSNV"]
		if "VEPappris" not in contents:
			sys.stderr.write("[WARNING] The configuration file does not contain the key 'VEPappris'. The annotated VCF will not be filtered according to the desired transcript.\n")
			contents["VEPappris"] = None
		elif not os.path.isfile(reference+"/"+contents["VEPappris"]): #checking whether APPRIS file exists:
			sys.stderr.write("\n[ERROR] The APPRIS file '"+reference+"/"+contents["VEPappris"]+"' does not exist.\n\n")
			sys.exit(1)
		else:
			contents["VEPappris"] = reference+"/"+contents["VEPappris"]
		if "PBSVtrf" not in contents:
			sys.stderr.write("[WARNING] The configuration file does not contain the key 'PBSVtrf'. It is highly recommended to provide one tandem repeat annotation for pbsv. The algorithm pbsv will be called without this annotation (less accurate).\n")
			contents["PBSVtrf"] = None
		elif not os.path.isfile(reference+"/"+contents["PBSVtrf"]): #checking whether TRF file exists:
			sys.stderr.write("\n[ERROR] The TRF file '"+reference+"/"+contents["PBSVtrf"]+"' does not exist.\n\n")
			sys.exit(1)
		else:
			contents["PBSVtrf"] = reference+"/"+contents["PBSVtrf"]
		if "PBSVtypes" not in contents:
			sys.stderr.write("[WARNING] The configuration file does not contain the key 'PBSVtypes'. The algorithm pbsv will call all possible structural variants (DEL,INS,INV,DUP,BND,CNV).\n")
			contents["PBSVtypes"] = "DEL,INS,INV,DUP,BND,CNV"
		else:
			svtypes = contents["PBSVtypes"].split(",")
			for svtype in svtypes:
				if svtype not in ["DEL", "INS", "INV", "DUP", "BND", "CNV"]:
					sys.stderr.write("\n[ERROR] The structural variant '"+svtype+"' provided in the configuration file under the key 'PBSVtypes' is not recognized.\n\n")
					sys.exit(1)
			contents["PBSVtypes"] = contents["PBSVtypes"]
		filesFormat = None #fq, fq.gz, bam
		for sample in contents["samples"]: #for each sample
			for n in range(len(contents["samples"][sample])): #for each file
				if not os.path.isfile(path+"/"+contents["samples"][sample][n]): #checking whether the sequencing file exists
					sys.stderr.write("\n[ERROR] The sequencing file '"+path+"/"+contents["samples"][sample][n]+"' does not exist.\n\n")
					sys.exit(1)
				contents["samples"][sample][n] = path+"/"+contents["samples"][sample][n] #appending the path to the filename
				if contents["samples"][sample][n].endswith("fq") or contents["samples"][sample][n].endswith("fastq"):
					fileFormat = "fastq"
				elif contents["samples"][sample][n].endswith("fq.gz") or contents["samples"][sample][n].endswith("fastq.gz"):
					fileFormat = "compressed fastq"
				elif contents["samples"][sample][n].endswith("bam"):
					fileFormat = "bam"
				else:
					sys.stderr.write("\n[ERROR] Only .fq/.fastq/.fq.gz/.fastq.gz/.bam extensions are allowed for the sequencing data. The following file has none of the mentioned extensions: '"+contents["samples"][sample][n]+"'.\n\n")
					sys.exit(1)
				if filesFormat == None:
					filesFormat = fileFormat
				elif filesFormat != fileFormat:
					sys.stderr.write("\n[ERROR] Please do not mix sequencing data under different formats/extensions: '"+filesFormat+"' and '"+fileFormat+"' detected.\n\n")
					sys.exit(1)
		pbsv = {"trf": contents["PBSVtrf"], "types": contents["PBSVtypes"]}
		vep = {"species": contents["VEPspecies"], "assembly": contents["VEPassembly"], "dbNSFP": contents["VEPdbNSFP"], "dbscSNV": contents["VEPdbscSNV"], "appris": contents["VEPappris"]}
		return contents["samples"], contents["genome"], contents["target"], filesFormat, vep, pbsv

	def mergeFastqSamples(self, output):
		""" This function will merge samples split into different files (FASTQ format) """
		for sample in self.samples:
			if len(self.samples[sample]) == 1:
				#nothing to be merged, just 1 file for this sample
				continue
			print("["+getTime()+"] Merging sample "+sample)
			try: #create directory if it does not exist
				os.mkdir(output+"/01-Merged_input_data")
			except:
				pass
			#merged data will go to the directory $output/Merged_input_data
			o = gzip.open(output+"/01-Merged_input_data/"+sample+".fq.gz", "wb") #output file
			for file_ in self.samples[sample]:
				if file_.endswith(".gz"):
					f = gzip.open(file_, "rb") #input file if compressed
				else:
					f = open(file_, "rb") #input file if not compressed
				for line in f:
					o.write(line)
				f.close()
			o.close()
			#we do not need any more the split samples, so we replace the corresponding
			#file in the dictionary samples
			self.samples[sample] = [output+"/01-Merged_input_data/"+sample+".fq.gz"]
		return self

	def mergeBamSamples(self, output, threads, verbose):
		""" This function will merge samples split into different files (BAM format) """
		commands = []
		for sample in self.samples:
			if len(self.samples[sample]) == 1:
				#nothing to be merged, just 1 file for this sample
				continue
			print("["+getTime()+"] Merging sample "+sample)
			try: #create directory if it does not exist
				os.mkdir(output+"/01-Merged_input_data")
			except:
				pass
			#merged data will go to the directory $output/Merged_input_data
			commands.append([
				"samtools",
				"merge",
				"-@", str(threads),
				"-f",
				output+"/01-Merged_input_data/"+sample+".bam"
			] + self.samples[sample])
			#we do not need any more the split samples, so we replace the corresponding
			#file in the dictionary samples
			self.samples[sample] = [output+"/01-Merged_input_data/"+sample+".bam"]
		executeCommands(1, commands, verbose, None)
		return self

	def fastqToBam(self, threads, verbose):
		""" This function will convert FASTQ files to BAM files. It expects each sample to have
		just 1 file. In this context, this function should be called after mergeFastqSamples()."""
		print("["+getTime()+"] Converting FASTQ data to BAM format")
		commands = []
		for sample in self.samples:
			sfile = self.samples[sample][0].split(".")
			if self.format == "compressed fastq": #file is compressed
				BAM = ".".join(sfile[0:len(sfile)-2])+".bam" #output BAM file
			#building the command
			commands.append([
				"picard",
				"FastqToSam",
				"F1="+self.samples[sample][0],
				"O="+BAM,
				"SM="+sample
			])
			#replacing default file for the given sample
			self.samples[sample] = [BAM]
		executeCommands(threads, commands, verbose, None)
		return self

	def __init__(self, path, reference, bed):
		# self.files = self.getFiles(path)
		# self.samples = self.getSamples(self.files)
		self.samples, self.genome, self.target, self.format, self.vep, self.pbsv = self.readConfig(path, reference, bed)

def getTime():
	""" This function checks the current time and outputs hour:minutes:seconds.
	This function is used for logging purposes. """
	date = datetime.datetime.now()
	hour = str(date.hour)
	minute = str(date.minute)
	second = str(date.second)
	if len(hour) != 2:
		hour = "0"+hour
	if len(minute) != 2:
		minute = "0"+minute
	if len(second) != 2:
		second = "0"+second
	hhmmss = hour+":"+minute+":"+second
	return hhmmss

def inputPar():
	""" Parsing arguments to the script """
	parser = argparse.ArgumentParser(description="Variant calling for PacBio data")
	parser.add_argument(
		"-i", "--input",
		metavar = "DIR",
		type = input_path,
		required = True,
		help = "Directory that contains the sequencing data as well as the configuration file 'config.json' [required].")
	parser.add_argument(
		"-r", "--reference",
		metavar = "DIR",
		type = input_path,
		required = True,
		help = "Directory that contains the reference genome [required].")
	parser.add_argument(
		"-b", "--bed",
		metavar = "DIR",
		type = input_path,
		required = True,
		help = "Directory that contains the BED file with the target regions [required].")
	parser.add_argument(
		"-o", "--output",
		metavar = "DIR",
		type = output_path,
		required = True,
		help = "Output directory to store the results [required].")
	parser.add_argument(
		"-t", "--threads",
		metavar = "INT",
		type = int,
		default = 1,
		required = False,
		help = "Number of CPUs to use [default 1].")
	parser.add_argument(
		"--snpcov",
		metavar = "INT",
		type = int,
		default = 15,
		required = False,
		help = "Minimum read coverage for a SNP to be called [default 15].")
	parser.add_argument(
		"--snpqual",
		metavar = "INT",
		type = int,
		default = 30,
		required = False,
		help = "Minimum quality for a SNP to be called [default 30].")
	parser.add_argument(
		"--svcov",
		metavar = "INT",
		type = int,
		default = 5,
		required = False,
		help = "Minimum read coverage for a SV to be called [default 5].")
	parser.add_argument(
		"-v", "--verbose",
		action = "store_true",
		help = "Show detailed information about the processes (e.g. commands) in the standard output.")
	parser.add_argument(
		"-d", "--docker",
		action = "store_true",
		help = argparse.SUPPRESS)
	return parser.parse_args()

# def input_path(string):
# 	""" This is an auxiliary function for inputPar() and checks whether an input
# 	file exists. """
# 	if os.path.isfile(string):
# 		return string
# 	sys.stderr.write("\n[ERROR] The input configuration file '"+string+"/config.txt' does not exist.\n\n")
# 	sys.exit(1)

def input_path(string):
	""" This is an auxiliary function for inputPar() and checks whether an input
	directory exists. If not, it throws an error. """
	print(string)
	if os.path.isdir(string):
		return string
	sys.stderr.write("\n[ERROR] Input directory '"+string+"' does not exist.\n\n")
	sys.exit(1)

def output_path(string):
	""" This is an auxiliary function for inputPar() and checks whether the output
	directory exists. If not, it creates the directory. """
	if os.path.isdir(string):
		return string
	try:
		os.mkdir(string)
	except:
		sys.stderr.write("\n[ERROR] The output directory '"+string+"' could not be created. Please, check if you have the corresponding write permissions.\n\n")
		sys.exit(1)
	return string

def getCommandsFastQC(threads, samples, output):
	""" Building FastQC commands and creating output directory for them """
	print("["+getTime()+"] Performing quality check on the data with FastQC")
	output = output+"/02-Quality_check"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	commands = []
	for sample in samples:
		commands.append([
			"fastqc",
			"-t", "20", #each thread, 250Mb RAM, -> 5Gb RAM each file
			"-o", output,
			"--extract",
			samples[sample][0]
		])
	return commands

def getCommandsMultiQC(threads, output):
	""" Building MultiQC commands """
	print("["+getTime()+"] Performing quality check on the data with MultiQC")
	output = output+"/02-Quality_check"
	commands = [[
		"multiqc",
		"-f",
		"-o", output,
		output
	]]
	return commands

def getCommandsPbmm2(threads, samples, genome, output, verbose):
	""" Building pbmm2 commands """
	print("["+getTime()+"] Mapping the data with pbmm2")
	output = output+"/03-Mapping"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#checking whether the reference index exists
	sgenome = genome.split(".")
	# if sgenome[-1] == "gz":
	# 	prefix = ".".join(sgenome[0:len(sgenome)-2])
	# 	isRefGzipped = True
	# else:
	prefix = ".".join(sgenome[0:len(sgenome)-1])
	# 	isRefGzipped = False
	if not os.path.isfile(prefix+".mmi"): #does not exist!
		sys.stderr.write("[WARNING] The reference index was not found. Generating it...\n")
		# if isRefGzipped: #in order to create the index, we need to uncompress it first
		# 	executeCommands(1, [["gunzip", genome]], verbose, None)
		# 	genome = ".".join(sgenome[0:len(sgenome)-1]) #the uncompressed genome will have the same name but without '.gz'
		#command to index the uncompressed FASTA file
		executeCommands(1, [["pbmm2", "index", "-j", str(threads), genome, prefix+".mmi"]], verbose, None)
		# if isRefGzipped: #if the input genome was initially compressed, let's compress it with bgzip
		# 	executeCommands(1, [["bgzip", "-@", str(threads), genome]], verbose, None)
	#building commands
	commands = []
	for sample in samples:
		commands.append([
			"pbmm2",
			"align",
			prefix+".mmi",
			samples[sample][0],
			output+"/"+sample+".bam",
			"--sort",
			"--median-filter",
			"--sample", sample,
			"-j", str(threads),
			"--unmapped"
		]) #--rg '@RG\tID:myid\tSM:mysample'
	return commands

def getCommandsLongshot(threads, samples, genome, targets, output, verbose):
	""" Building longshot commands """
	print("["+getTime()+"] Variant calling with longshot")
	input_ = output+"/03-Mapping"
	output = output+"/05-Variant_calling"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#checking whether the FASTA index exists
	if not os.path.isfile(genome+".fai"): #does not exist!
		executeCommands(1, [["samtools", "faidx", genome]], verbose, None) #creating FASTA index
	#building commands
	commands = []
	f = open(targets, "r") #reading targets file
	for line in f: #for each region
		sline = line.rstrip("\n").split("\t")
		region = sline[0]+":"+sline[1]+"-"+sline[2] #region in which we have to perform the variant calling
		for sample in samples: #for each sample
			commands.append([
				"longshot",
				"-F",
				"-A",
				"-r", region,
				"--ref", genome,
				"--bam", input_+"/"+sample+".bam",
				"--out", output+"/"+sample+".longshot."+region+".vcf"
			])
	f.close()
	# print(commands)#da valutare "-a 6.0",
	return commands

def getCommandsDeepVariant(threads, samples, genome, targets, output, verbose):
	""" Building longshot commands """
	print("["+getTime()+"] Variant calling with DeepVariant")
	input_ = output+"/03-Mapping"
	output = output+"/05-Variant_calling_DV"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#checking whether the FASTA index exists
	if not os.path.isfile(genome+".fai"): #does not exist!
		executeCommands(1, [["samtools", "faidx", genome]], verbose, None) #creating FASTA index
	#building commands
	commands = []
	f = open(targets, "r") #reading targets file
	for line in f: #for each region
		sline = line.rstrip("\n").split("\t")
		region = sline[0]+":"+sline[1]+"-"+sline[2] #region in which we have to perform the variant calling
		for sample in samples: #for each sample
			commands.append([
				"longshot",
				"-F",
				"-A",
				"-a 6.0"
				"-r", region,
				"--ref", genome,
				"--bam", input_+"/"+sample+".bam",
				"--out", output+"/"+sample+".longshot."+region+".vcf"
			])
	f.close()
	return commands

def getCommandsPbsv(threads, samples, genome, pbsvArgs, targets, output, verbose):
	""" Building pbsv commands """
	print("["+getTime()+"] Variant calling with pbsv")
	input_ = output+"/03-Mapping"
	output = output+"/05-Variant_calling"
	#building commands
	commands1 = []
	commands2 = []
	f = open(targets, "r") #reading targets file
	for line in f: #for each region
		sline = line.rstrip("\n").split("\t")
		region = sline[0]+":"+sline[1]+"-"+sline[2] #region in which we have to perform the variant calling
		for sample in samples:
			#pbsv discover
			commands1.append([
				"pbsv",
				"discover",
				"--region", region,
				input_+"/"+sample+".bam",
				output+"/"+sample+".pbsv."+region+".svsig.gz"
			])
			if pbsvArgs["trf"] != None: #PBSVtrf argument specified in the configuration file
				commands1[-1].append("--tandem-repeats")
				commands1[-1].append(pbsvArgs["trf"])
			else:
				sys.stderr.write("[WARNING] The annotation of tandem repeats ('"+prefix+".trf.bed') does not exist. It is highly recommended to provide one tandem repeat annotation for pbsv.\n")
			#pbsv call
			commands2.append([
				"pbsv",
				"call",
				"-j", str(threads),
				"-t", pbsvArgs["types"],
				genome,
				output+"/"+sample+".pbsv."+region+".svsig.gz",
				output+"/"+sample+".pbsv."+region+".vcf"
			])
			#some cleaning
			commands2.append([
				"rm", output+"/"+sample+".pbsv."+region+".svsig.gz"
			])
	f.close()
	return commands1, commands2

def getCommandsGeneralStatistics(threads, samples, genome, target, output, verbose):
	""" Building commands to retrieve general statistics """
	print("["+getTime()+"] Computing general statistics")
	input_ = output+"/03-Mapping"
	output = output+"/04-general_statistics"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#checking whether the reference dictionary for Picard exists
	sgenome = genome.split(".")
	prefix = ".".join(sgenome[0:len(sgenome)-1])
	if not os.path.isfile(prefix+".dict"): #does not exist!
		sys.stderr.write("[WARNING] The reference dictionary was not found. Generating it...\n")
		executeCommands(1, [["picard", "CreateSequenceDictionary", "R="+genome, "O="+prefix+".dict"]], verbose, None)
	#checking whether the interval list for Picard exists
	intervalList = target.split(".bed")[0]+".interval_list"
	if not os.path.isfile(intervalList): #does not exist!
		sys.stderr.write("[WARNING] The interval list was not found. Generating it...\n")
		executeCommands(1, [["picard", "BedToIntervalList", "I="+target, "O="+intervalList, "SD="+prefix+".dict"]], verbose, None)
	#building commands
	commands = []
	for sample in samples:
		#pbmm2 outputs unmapped reads with MAPQ 255, and it is not compatible
		#with Picard CollectHsMetrics, so we need to use a BAM without unmapped reads
		commands.append([
			"samtools",
			"view",
			"-b",
			"-F", str(4),
			"-@", str(threads),
			"-o", input_+"/"+sample+".mapped.bam",
			input_+"/"+sample+".bam"
		])
		#Picard CollectHsMetrics gets the statistics for the target regions
		commands.append([
			"picard",
			"CollectHsMetrics",
			"MINIMUM_BASE_QUALITY=0",
			"MINIMUM_MAPPING_QUALITY=0",
			"BAIT_INTERVALS="+intervalList,
			"TARGET_INTERVALS="+intervalList,
			"OUTPUT="+output+"/"+sample+"_PicardCollectHsMetrics.txt",
			"INPUT="+input_+"/"+sample+".mapped.bam"
		])
		#Qualimap gets general statistics about the mapping
		commands.append([
			"qualimap",
			"bamqc",
			"-bam", input_+"/"+sample+".bam",
			"-outdir", output+"/"+sample+"_qualimap",
			"-nt", str(threads),
			"--java-mem-size=8G",
			" -Djava.awt.headless=true"
		])
	return commands
#aggiunto  -Djava.awt.headless=true perchè $DISPLAY è settata a 10.75.75.75:0.0

def getCommandsEnsemblVEPforSNP(threads, samples, genome, vep, output, verbose):
	""" Building Ensembl VEP commands """
	print("["+getTime()+"] Variant annotation with Ensembl VEP")
	input_ = output+"/05-Variant_calling"
	output = output+"/06-Variant_annotation"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#building commands
	commands = []
	for sample in samples:
		commands.append([
			"vep",
			"--cache",
			"--refseq",
			"--offline",
			"--species", vep["species"],
			"--assembly", vep["assembly"],
			"--fasta", genome,
			"--dir", os.path.dirname(genome),
			"--fork", str(threads),
			"--force_overwrite",
			"--vcf",
			"--format", "vcf",
			"--everything",
			"--af_1kg",
			"--af_esp",
			"--buffer_size", "500",
			"--force",
			"--xref_refseq",
			"--exclude_predicted",
			"--use_transcript_ref",
			"-i", input_+"/"+sample+".longshot.filt.vcf",
			"-o", output+"/"+sample+".longshot.filt.vcf",
			"--use_given_ref"
		])
		if vep["dbNSFP"] != None:
			commands[-1].append("--plugin")
			commands[-1].append("dbNSFP,"+vep["dbNSFP"]+''',\
Eigen-PC-raw_coding_rankscore,\
CADD_raw_rankscore,\
DANN_rankscore,\
MetaSVM_rankscore,\
MetaLR_rankscore,\
FATHMM_converted_rankscore,\
MutationTaster_converted_rankscore,\
MutationAssessor_rankscore,\
Polyphen2_HDIV_rankscore,\
Polyphen2_HVAR_rankscore,\
SIFT_converted_rankscore,\
LRT_converted_rankscore,\
MutPred_rankscore,\
PROVEAN_converted_rankscore,\
VEST4_rankscore,\
REVEL_rankscore,\
SIFT_pred,\
Polyphen2_HDIV_pred,\
Polyphen2_HVAR_pred,\
LRT_pred,\
MutationTaster_pred,\
MutationAssessor_pred,\
FATHMM_pred,\
PROVEAN_pred,\
MetaSVM_pred,\
MetaLR_pred,\
M-CAP_pred,\
fathmm-MKL_coding_pred,\
GERP++_RS_rankscore,\
phyloP100way_vertebrate_rankscore,\
phyloP30way_mammalian_rankscore,\
phyloP30way_mammalian_rankscore,\
phastCons100way_vertebrate_rankscore,\
phastCons30way_mammalian_rankscore,\
SiPhy_29way_logOdds_rankscore,\
clinvar_clnsig,\
clinvar_review,\
Interpro_domain,\
gnomAD_exomes_POPMAX_AF,\
gnomAD_exomes_POPMAX_nhomalt,\
gnomAD_exomes_controls_AF,\
gnomAD_exomes_controls_AC,\
gnomAD_genomes_POPMAX_AF,\
gnomAD_genomes_POPMAX_nhomalt,\
gnomAD_genomes_AC,\
clinvar_review,\
clinvar_MedGen_id,\
clinvar_OMIM_id,\
clinvar_Orphanet_id,\
gnomAD_exomes_controls_nhomalt''')
		if vep["dbscSNV"] != None:
			commands[-1].append("--plugin")
			commands[-1].append("dbscSNV,"+vep["dbscSNV"]+''',\
GRCh38,\
ada_score,\
rf_score''')
	return commands

def getCommandsEnsemblVEPforSV(threads, samples, genome, vep, output, verbose):
	""" Building Ensembl VEP commands """
	input_ = output+"/05-Variant_calling"
	output = output+"/06-Variant_annotation"
	try: #create directory if it does not exist
		os.mkdir(output)
	except: #probably it is aleady created
		pass
	#building commands
	commands = []
	for sample in samples:
		commands.append([
			"vep",
			"--cache",
			"--refseq",
			"--offline",
			"--species", vep["species"],
			"--assembly", vep["assembly"],
			"--fasta", genome,
			"--dir", os.path.dirname(genome),
			"--fork", str(threads),
			"--force_overwrite",
			"--vcf",
			"--format", "vcf",
			"--biotype",
			"--buffer_size", "500",
			"--force",
			"--xref_refseq",
			"--exclude_predicted",
			"--use_transcript_ref",
			"-i", input_+"/"+sample+".pbsv.filt.vcf",
			"-o", output+"/"+sample+".pbsv.filt.vcf",
			"--use_given_ref"
		])
	return commands

def checkRunningCommands(runningCommands, verbose, pattern):
	""" This function checks whether some executed commands successfully finished.
	In this case, it returns a dictionary with the actual running commands. """
	brokenLoop = False
	for id_ in runningCommands:
		#cheching if any process has finished
		if runningCommands[id_].poll() != None: #command finished
			out, err = runningCommands[id_].communicate()
			err = out #STDERR piped to STDOUT
			err = err.decode("ascii") #bytes to string
			if runningCommands[id_].returncode != 0 or (pattern != None and pattern in err):
				#if finished with a non-zero exit code, the command was not succesful
				#Note: the idea of $pattern is that there is software (e.g. FastQC) that
				#sometimes fails with exit code 0
				sys.stderr.write(err+"\nCommand exited with non-zero status.\n")
				sys.exit(1) #script finishes here with an exit code of 1
			if verbose: #if '--verbose' set in arguments, let's print the standard error
				if err != "": #the command did not return anything as stderr or stdout
					sys.stderr.write(err+"\n")
			brokenLoop = True
			break
	if brokenLoop: #some command successfully finished
		del runningCommands[id_]
	time.sleep(1) #sleep 1 second
	return runningCommands

def executeCommands(procs, commands, verbose, pattern):
	""" This function handles the number of commands to start depending on
	the maximum number of threads the user input. """
	global docker
	runningCommands = {}
	count = 0
	for command in commands: #for each command in array of commands
		if docker and command[0] == "vep": #executing ensembl-vep in Docker
			#ensembl-vep was installed into the conda environment packages2,
			#separated from the other packages, which are installed into the
			#environment packages1, due to conflicting dependencies
			command = ["conda", "run", "-n", "packages2"] + command
		elif docker: #executing the other packages in Docker
			command = ["conda", "run", "-n", "packages1"] + command
		count += 1 #ID of the analysis in dictionary runningCommands
		if verbose: #if '--verbose' set in arguments, let's print the executed command
			sys.stderr.write("[COMMAND] "+" ".join(command)+"\n")
		runningCommands[count] = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		while len(runningCommands) == procs: #if number of executed commands is equal to max number of processes
			runningCommands = checkRunningCommands(runningCommands, verbose, pattern) #check if there are some commands that finished
	while len(runningCommands) != 0: #we wait here for all commands to finish
		runningCommands = checkRunningCommands(runningCommands, verbose, pattern)

def filterLongshotVCF(samples, output, cov_threshold, qual_threshold, verbose):
	""" This function will filter the raw VCF from Longshot """
	print("["+getTime()+"] Filtering longshot VCF file")
	input_ = output+"/05-Variant_calling"
	output = input_
	pattern_cov = re.compile("DP=([0-9]*);")
	for sample in samples:
		fi = open(input_+"/"+sample+".longshot.vcf", "r")
		fo = open(input_+"/"+sample+".longshot.filt.vcf", "w")
		for line in fi:
			if line.startswith("#"): #header/metadata
				fo.write(line)
				continue
			#variants
			sline = line.rstrip("\n").split("\t")
			cov = pattern_cov.search(sline[7])
			if cov == None:
				sys.stderr.write("[ERROR] Could not determine the depth coverage for the variant:\n"+line)
				sys.exit(1)
			cov = cov.group(1)
			if int(cov) >= cov_threshold and float(sline[5]) >= qual_threshold and sline[6] == "PASS":
				fo.write(line)
		fi.close()
		fo.close()

def filterPbsvVCF(samples, output, cov_threshold, verbose):
	""" This function will filter the raw VCF from pbsv """
	print("["+getTime()+"] Filtering pbsv VCF file")
	input_ = output+"/05-Variant_calling"
	output = input_
	for sample in samples:
		fi = open(input_+"/"+sample+".pbsv.vcf", "r")
		fo = open(input_+"/"+sample+".pbsv.filt.vcf", "w")
		for line in fi:
			if line.startswith("#"): #header/metadata
				fo.write(line)
				continue
			#variants
			sline = line.rstrip("\n").split("\t")
			vcfformat = sline[8].split(":")
			if vcfformat[2] != "DP":
				sys.stderr.write("[ERROR] Could not determine the depth coverage for the variant:\n"+line)
				sys.exit(1)
			cov = sline[9].split(":")[2]
			if int(cov) >= cov_threshold and sline[6] == "PASS":
				fo.write(line)
		fi.close()
		fo.close()

def filterEnsemblVEP(samples, output, appris):
	""" Filtering VEP annotation with APPRIS file """
	print("["+getTime()+"] Filtering variant annotation")
	input_ = output+"/06-Variant_annotation"
	output = input_
	#read appris file
	apprisDict = {}
	f = open(appris, "r")
	for line in f:
		sline = line.rstrip("\n").split("\t")
		if sline[0] == "GENE" or sline[0].startswith("#"): #header
			continue
		if sline[1] in apprisDict:
			sys.stderr.write("[WARNING] Gene "+sline[0]+" repeated in the APPRIS file.\n")
		accession_without_version = sline[1].split(".")
		accession_without_version = ".".join(accession_without_version[0:len(accession_without_version)-1])
		apprisDict[sline[0]] = accession_without_version
	f.close()
	#read vcf files
	genes_not_in_appris = [] #this list will store genes that are not available in the provided APPRIS file in order to show the corresponding warning
	for sample in samples:
		fi = open(input_+"/"+sample+".longshot.filt.vcf", "r")
		fo = open(input_+"/"+sample+".longshot.filt.appris.vcf", "w")
		for line in fi:
			if line.startswith("#"): #header of the VCF file
				fo.write(line)
				continue
			sline = line.rstrip("\n").split("\t")
			CSQ = sline[7].split(";CSQ=")
			INFO = CSQ[0]+";CSQ=" #all elements from INFO field excluding VEP annotations
			CSQ = CSQ[1].split(",") #list of all VEP annotations
			geneDict = {} #gene as key, value is list of annotations
			finalCSQ = "" #final CSQ field that will be written
			#looping over VEP annotations for the current variant
			for annotation in CSQ:
				sannotation = annotation.split("|")
				if sannotation[3] == "": #variant that is not on a gene
					if finalCSQ == "":
						finalCSQ = annotation
					else:
						finalCSQ = finalCSQ + "," + annotation
					break
				if sannotation[3] not in geneDict:
					geneDict[sannotation[3]] = [sannotation]
				else:
					geneDict[sannotation[3]].append(sannotation)
			#looping over VEP annotations gene-wise
			for gene in geneDict:
				finalAnnotation = ""
				for sannotation in geneDict[gene]:
					accession_without_version = sannotation[6].split(".")
					accession_without_version = ".".join(accession_without_version[0:len(accession_without_version)-1])
					if sannotation[3] in apprisDict and accession_without_version == apprisDict[sannotation[3]]:
						finalAnnotation = sannotation
						break
				if finalAnnotation == "" and gene not in genes_not_in_appris: #transcript not found in the APPRIS file, choosing the first annotation
					sys.stderr.write("[WARNING] None of the annotated transcripts from gene "+gene+" were found in the APPRIS file (variant "+sline[0]+":"+sline[1]+").\n")
					finalAnnotation = geneDict[gene][0]
					genes_not_in_appris.append(gene)
				if finalCSQ == "":
					finalCSQ = "|".join(finalAnnotation)
				else:
					finalCSQ = finalCSQ + "," + "|".join(finalAnnotation)
			#formatting the final output
			sline[7] = INFO+finalCSQ
			fo.write("\t".join(sline)+"\n")
		fi.close()
		fo.close()

def mergeVCF(samples, caller, output):
	""" This function will merge VCF files from the longshot/pbsv calling """
	input_ = output+"/05-Variant_calling"
	output = input_
	for sample in samples:
		pattern = (input_+"/"+sample+"."+caller+".*:*-*.vcf")
		files = [] #all files with the extensions above will be stored in this list
		files.extend(glob.glob(pattern))
		files.sort()
		fo = open(output+"/"+sample+"."+caller+".vcf", "w") #merged file
		for n in range(len(files)):
			file = files[n]
			fi = open(file, "r")
			for line in fi:
				sline = line.rstrip("\n").split("\t")
				if line.startswith("#") and n == 0:
					#header from the first VCF file
					fo.write(line)
				elif line.startswith("#"):
					pass
				else: #variants
					fo.write(line)
			fi.close()
			os.remove(file) #deleting file
		fo.close()

def main():
	""" This is the main function, and it will call all the other functions """
	global dirpath
	global docker #is running in Docker?
	#executing inputPar function to handle the arguments
	args = inputPar()
	docker = args.docker
	#parse the configuration file to identify sequencing data and the reference genome
	analysis = Analysis(args.input, args.reference, args.bed)
	if "fastq" in analysis.format: #input files are in FASTQ format
		analysis = analysis.mergeFastqSamples(args.output) #merging files in case of multiple files for a sample
		# analysis = analysis.fastqToBam(args.threads, args.verbose) #Note: pbmm2 requires either fastq or subread BAM files. FASTQ->BAM will not work.
	else: #BAM format
		analysis = analysis.mergeBamSamples(args.output, args.threads, args.verbose) #merging files in case of multiple files for a sample
	#quality check
	cmd = getCommandsFastQC(args.threads, analysis.samples, args.output)
	executeCommands(args.threads, cmd, args.verbose, "Failed to process file")
	cmd = getCommandsMultiQC(args.threads, args.output)
	executeCommands(1, cmd, args.verbose, None)
	#mapping
	cmd = getCommandsPbmm2(args.threads, analysis.samples, analysis.genome, args.output, args.verbose)
	executeCommands(1, cmd, args.verbose, None)
	#general statistics
	cmd = getCommandsGeneralStatistics(args.threads, analysis.samples, analysis.genome, analysis.target, args.output, args.verbose)
	executeCommands(1, cmd, args.verbose, None)
	#variant calling
	cmd1 = getCommandsLongshot(args.threads, analysis.samples, analysis.genome, analysis.target, args.output, args.verbose)
	cmd2, cmd3 = getCommandsPbsv(args.threads, analysis.samples, analysis.genome, analysis.pbsv, analysis.target, args.output, args.verbose)
	executeCommands(args.threads, cmd1+cmd2, args.verbose, None)
	executeCommands(1, cmd3, args.verbose, None)
	#merge VCF files
	mergeVCF(analysis.samples, "longshot", args.output)
	mergeVCF(analysis.samples, "pbsv", args.output)
	#filter VCF files
	filterLongshotVCF(analysis.samples, args.output, args.snpcov, args.snpqual, args.verbose)
	filterPbsvVCF(analysis.samples, args.output, args.svcov, args.verbose)
	#annotation
	cmd1 = getCommandsEnsemblVEPforSNP(args.threads, analysis.samples, analysis.genome, analysis.vep, args.output, args.verbose)
	cmd2 = getCommandsEnsemblVEPforSV(args.threads, analysis.samples, analysis.genome, analysis.vep, args.output, args.verbose)
	executeCommands(args.threads, cmd1+cmd2, args.verbose, None)
	if analysis.vep["appris"] != None:
		filterEnsemblVEP(analysis.samples, args.output, analysis.vep["appris"])

#executing the main function
main()
