#!/home/bioinfo/miniconda3/envs/pacbioenv/bin/python

# -*- coding: utf-8 -*-

import numpy as np
from multiprocessing import Process, Lock
import first_pacbio_core_V2 as step
import multiprocessing
import argparse
from os import listdir, system, getcwd,getenv
from os.path import isfile, join, exists
import datetime
import subprocess
import time
from os import getenv
import time,os,sys
import csv
import glob
import pandas as pd
#import allel
import os

path = getcwd()
reference = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE', 'all_chr38.fa')
appris_principal = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','structuralvariant.bed')
bed = join('/home/bioinfo/PROJECT/pacbio', 'dataset/REFERENCE','BED')
def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='MAGI EUREGIO DIAGNOSYS',description='Pipe from FASTQ to BAM',
			epilog='Need to BWA 0.7.12-r1039 and SAMTOOLS v1.3 (SET on global PATH or INSERT your path [--bwa] [--samtools]')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = Pipe run in same folder when launch command]')


	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Pannelli Consigliati: 1:CLM2; 2:OSSNEW; 3:OCULARE; 4:OCULARE2; 5:SIFSR; 6:ALLGENE;'
					'7:trusightone; 8:GENETEST1; 9:GENETEST2; 10:CANCER; 11:MALFORMATIONI VASCOLARI;'
					'12:ANOMALIE VASCOLARI; 13:INFERTILITA; 14:OBESITA; 15:GENODERMATOSI\nRequired')

	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z','p'],
	                    required=True,
	                    help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca; p = privato,  - required ')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert Family name to create Folder')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: ALLERT!!! Set on "False" overwrite old data [Default = True]')

#	parser.add_argument('-o','--name', metavar='Output ', required=True,help='Choose an output filename (Required)')

	return parser.parse_args()

def read_vcf(vcf_name):
    coln = ['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','SAMPLE']
    vcf = pd.read_csv(vcf_name, sep='\t',names = coln, comment = '#')
    print(vcf)
    return vcf

def final_annotation(vcf_l, APPRIS, path, sample):

    	# sample_x+'_samt',sample_x+'_gatk','GENE','exone',
        #             'length','strand','refseq','hgmd'
    vcf_l[sample + '_samt'] = vcf_l['SAMPLE']
    vcf_l[sample + '_gatk'] = None

    vcf_l['GENE'] = vcf_l['INFO'].str.split('|').str.get(3)
    vcf_l['exone'] = None
    vcf_l['length'] = None
    vcf_l['strand'] = None
    vcf_l['refseq'] = None
    vcf_l['hgmd'] = None
    vcf_l['INFO3'] = vcf_l['INFO'].str.split('|,').str.get(0)

    INFOFIRST = pd.DataFrame(vcf_l['INFO'].str.split('CSQ=').str.get(0)) #tutto cio che c'è prima di csq
    INFOFIRST['INFOFIRST'] = INFOFIRST['INFO']
    INFOFIRST.drop('INFO',axis=1,inplace=True)
    print(INFOFIRST)

    INFO = vcf_l['INFO'].str.split('CSQ=').str.get(1) #tutto ciò che c'è dopo csq
    print(INFO)
    #INFO2 = pd.DataFrame(INFO.apply(lambda x: str(x).split('|,')))
    INFO2 = pd.DataFrame(INFO.apply(lambda x: str(x).split(','))) #splitto dalla , che segnala le diverse annotazioni della var
    INFO2['INFO2'] = INFO2['INFO']
    INFO2.drop('INFO',axis=1,inplace=True)
    print(INFO2.iloc[0,:]) #creo una lista per ogni variante dei diversi risultati dal vep

    INFOFIRST.reset_index(inplace=True)
    INFO2.reset_index(inplace=True)
    vcf_l.reset_index(inplace=True)

    vcf_l2 = pd.merge(vcf_l,INFOFIRST,on='index',how='left')
    result3 = pd.merge(vcf_l2,INFO2,on='index',how='left')
    result3['PRINCIPAL'] = None
    print(result3['SAMPLE'])


    REFSEQPRINCIPAL = APPRIS['refseq'].str.split('.').str.get(0)+'.'


    #print(REFSEQPRINCIPAL)

    for index,row in result3.iterrows():
        for x in row.INFO2: #prendo ogni elemento della lista
            for refseq in REFSEQPRINCIPAL:
                if refseq in x:
                    if (APPRIS['GENE'][APPRIS['refseq'].str.split('.').str.get(0)+'.' == refseq]==row['GENE']).item():
                        result3.loc[index,'PRINCIPAL'] = str(row['INFOFIRST'])+'CSQ='+x
                    else: pass
    # print(vcf_l.loc[0, 'INFO'])
    #print(result3.loc[0, 'PRINCIPAL'])
    result3['PRINCIPAL2'] = np.where(result3['PRINCIPAL'].isnull(),
                    result3['INFO3'],np.nan)

    result3['INFO'] = result3['PRINCIPAL']
    result3['INFO'].fillna(result3['PRINCIPAL2'],inplace=True)
    result3.drop('PRINCIPAL',axis=1,inplace=True)
    result3.drop('INFOFIRST',axis=1,inplace=True)
    result3.drop('INFO2',axis=1,inplace=True)
    result3.drop('INFO3',axis=1,inplace=True)
    result3.drop('PRINCIPAL2',axis=1,inplace=True)
    final = adapt_annotation2(path,sample,result3)#,HGMD)

    return result3


def adapt_annotation2(folder,sample,result):#,HGMD):
	name = str(sample)

	samtools = name+'_samt'
	gatk = name+'_gatk'


	# cols = ['sample','HGVS','CHROM','POS','GENE','ALTGENE','ID','types','QUAL','DEPTH','mapquality','INHERITANCE',
	# 	'samtools_geno','gatk_geno','unbalance','consequence','impact','clin_sign','strand',
	# 	'refseq','hgmd','num_exon','num_intron','HGVS_c','HGVS_p','CDS_position',
	# 	'amino_acids','codons','variation_cosmic','sift','polyphen','GMAF','ExAC_MAF','MAX_MAF','Adj_MAF','AFR_MAF','AMR_MAF','EAS_MAF',
	# 	'EUR_MAF','SAS_MAF','AA_MAF','EA_MAF','pubmed',
	# 	'hgmd_mutation','hgmd_function','hgmd_phenotype','hgmd_pubmed',
    #
	# 	'CADD_rankscore','DANN_rankscore','EigenPC_rankscore','FATHMM_rankscore','FATHMM_pred','GERP_rankscore','Interpro_domain',
	# 	'LRT_rankscore','LRT_pred','MCAP_pred','MetaLR_pred','MetaLR_rankscore','MetaSVM_pred','MetaSVM_rankscore',
	# 	'MutPred_rankscore','MutationAssessor_pred','MutationAssessor_rankscore','MutationTaster_rankscore','MutationTaster_pred',
	# 	'PROVEAN_rankscore','PROVEAN_pred','Polyphen2HDIV_pred','Polyphen2HDIV_rankscore','Polyphen2HVAR_pred',
	# 	'Polyphen2HVAR_rankscore','REVEL_rankscore','SIFT_rankscore','SIFT_pred','SiPhy29way_rankscore',
	# 	'VEST3_rankscore','clinvar_clnsig','fathmmMKL_pred',
	# 	'phastCons100way_rankscore','phastCons20way_rankscore','phyloP100way_rankscore','phyloP20way_rankscore',
    #
	# 	'ada_score','rf_score',
    #
	# 	'clinvar_MedGen_id','clinvar_OMIM_id','clinvar_Orphanet_id','clinvar_clnsig','clinvar_review',
	# 	'gnomAD_exomes_POPMAX_AF','gnomAD_exomes_POPMAX_nhomalt','gnomAD_exomes_controls_AF',
	# 	'gnomAD_exomes_controls_nhomalt','gnomAD_genomes_controls_nhomalt','gnomAD_genomes_POPMAX_AF','gnomAD_genomes_POPMAX_nhomalt',
	# 	'gnomAD_genomes_controls_POPMAX_AF','gnomAD_genomes_controls_POPMAX_nhomalt','gnomAD_exomes_controls_AC',
	# 	'gnomAD_genomes_controls_AC'
	# 	]


	cols = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO',
           'FORMAT', samtools, 'geno_descripion', 'geno_phasato', 'geno_info', 'GENE', 'strand',
           'DEPTH', 'consequence', 'impact', 'symbol', 'num_exon', 'num_intron',
           'HGVS_c', 'HGVS_p', 'cDNA_position', 'CDS_position', 'protein_position',
           'amino_acids', 'codons', 'variation', 'variation2', 'cosmic',
           'distance', 'hgnc_id', 'canonical', 'gene_pheno', 'sift', 'polyphen',
           'domain', 'GMAF', 'AFR_MAF', 'AMR_MAF', 'EAS_MAF', 'EUR_MAF', 'SAS_MAF',
           'AA_MAF', 'EA_MAF', 'ExAC_MAF', 'MAX_MAF', 'Adj_MAF', 'clin_sign',
           'somatic', 'PHENO', 'pubmed']
	#print result[['#CHROM','POS','REF','ALT','C%', 'G%','T%','A%','ins%','del%']]'samtools',
######################################################################################################
	if len(result) != 0:
		for index,row in result.iterrows():
			try:
				if ((row['G%'] != 0.0) & (row['ALT'] == 'G')):
					result.loc[index,'unbalance'] = 'G='+unicode(row['G%'])
				elif ((row['C%'] != 0.0) & (row['ALT'] == 'C')):
					result.loc[index,'unbalance'] = 'C='+unicode(row['C%'])
				elif ((row['T%'] != 0.0) & (row['ALT'] == 'T')):
					result.loc[index,'unbalance'] = 'T='+unicode(row['T%'])
				elif ((row['A%'] != 0.0) & (row['ALT'] == 'A')):
					result.loc[index,'unbalance'] = 'A='+unicode(row['A%'])
				if ((row['ins%'] != 0.0)):
					result.loc[index,'unbalance'] = 'ins='+unicode(row['ins%'])
				if ((row['del%'] != 0.0)):
					result.loc[index,'unbalance'] = 'del='+unicode(row['del%'])

				result.loc[index,'DEPTH2'] = row['sum']
			except KeyError:
				print ('TROVA VARIANTE TERMINATO!!!')
	#print result[['#CHROM','POS','REF','ALT','unbalance']]
######################################################################################################

		#result['samtools'] = result[samtools].str.split(':').str.get(0)
		result['geno_phasato'] = result[samtools].str.split(':').str.get(0)
		result['geno_info'] = result[samtools].str.split(':').str.get(2)
		result['gatk'] = result[gatk].str.split(':').str.get(0)

		result['DEPTH'] = np.where(result['INFO'].str.split(';').str.get(0) == 'INDEL',
					result['INFO'].str.split(';').str.get(3).str.split('=').str.get(1),
					result['INFO'].str.split(';').str.get(0).str.split('=').str.get(1))
		# print(result.loc[0, 'INFO'])
		# print((result.loc[0, 'INFO'].split('CSQ=')[0].split('MQ=')))#[1].split(';')[0]))
		# #result['mapquality'] = result['INFO'].str.split('CSQ=').str.get(0).str.split('MQ=').str.get(1).str.split(';').str.get(0)
		result['mapquality'] = None

		result['consequence'] = result['INFO'].str.split('|').str.get(1)
		result['impact'] = result['INFO'].str.split('|').str.get(2)
		result['symbol'] = result['INFO'].str.split('|').str.get(4)
		#result['transcript_id'] = result['INFO'].str.split('|').str.get(6)
		result['num_exon'] = result['INFO'].str.split('|').str.get(8)
		result['num_intron'] = result['INFO'].str.split('|').str.get(9)
		result['HGVS_c'] = result['INFO'].str.split('|').str.get(10)
		result['HGVS_p'] = result['INFO'].str.split('|').str.get(11)
		result['cDNA_position'] = result['INFO'].str.split('|').str.get(12)
		result['CDS_position'] = result['INFO'].str.split('|').str.get(13)
		result['protein_position'] = result['INFO'].str.split('|').str.get(14)
		result['amino_acids'] = result['INFO'].str.split('|').str.get(15)
		result['codons'] = result['INFO'].str.split('|').str.get(16)

		result['variation'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(0)
		try: result['variation2'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(1)
		except: result['variation2'] = ''

		result['variation'].fillna('',inplace=True)
		result['variation2'].fillna('',inplace=True)

		#if str(name)=='73.2017':
		# print result[['variation','variation2']]

		try:
			result['ID'] = np.where(result['variation'].str.contains('rs'),result['variation'],
                                        np.where(result['variation2'].str.contains('rs'),result['variation2'],np.nan))
		except:
			print ('ERROR IN NP.WHERE!!!!')
			result['ID'] = np.nan
		result['cosmic'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(1)
		result['cosmic'].fillna('',inplace=True)

		try:
			result['variation_cosmic'] = np.where(result['cosmic'].str.contains('COSM'),result['cosmic'],np.nan)
		except:
			print( 'ERROR COSMIC IN NP.WHERE!!!!')
			result['variation_cosmic'] = np.nan

		result['distance'] = result['INFO'].str.split('|').str.get(18)
		result['strand'] = result['INFO'].str.split('|').str.get(19)

		result['hgnc_id'] = result['INFO'].str.split('|').str.get(23)
		result['canonical'] = result['INFO'].str.split('|').str.get(24)

		#result['mane'] = result['INFO'].str.split('|').str.get(25)

		result['gene_pheno'] = result['INFO'].str.split('|').str.get(37)
		result['sift'] = result['INFO'].str.split('|').str.get(38)
		result['polyphen'] = result['INFO'].str.split('|').str.get(39)
		result['domain'] = result['INFO'].str.split('|').str.get(40)

		result['GMAF'] = result['INFO'].str.split('|').str.get(43)
		result['AFR_MAF'] = result['INFO'].str.split('|').str.get(52)
		result['AMR_MAF'] = result['INFO'].str.split('|').str.get(53)
		result['EAS_MAF'] = result['INFO'].str.split('|').str.get(55)
		result['EUR_MAF'] = result['INFO'].str.split('|').str.get(57)
		result['SAS_MAF'] = result['INFO'].str.split('|').str.get(59)

		result['AA_MAF'] = result['INFO'].str.split('|').str.get(49)
		result['EA_MAF'] = result['INFO'].str.split('|').str.get(50)

		#result['ExAC_MAF'] = result['INFO'].str.split('|').str.get(47)
		#result['Adj_MAF'] = result['INFO'].str.split('|').str.get(48)

		result['ExAC_MAF'] = result['INFO'].str.split('|').str.get(51) #GnomadAF
		result['MAX_MAF'] = result['INFO'].str.split('|').str.get(60)  #MAX_MAF
		result['Adj_MAF'] = result['INFO'].str.split('|').str.get(61)  #MAX_AF_POPS

		result['clin_sign'] = result['INFO'].str.split('|').str.get(62)
		result['somatic'] = result['INFO'].str.split('|').str.get(63)
		result['PHENO'] = result['INFO'].str.split('|').str.get(64)
		result['pubmed'] = result['INFO'].str.split('|').str.get(65)
		result['pubmed'] = result['pubmed'].str.split('&').str.get(0)+' '+result['pubmed'].str.split('&').str.get(1)+' '+result['pubmed'].str.split('&').str.get(2)

		try:result['CADD_rankscore'] = result['INFO'].str.split('|').str.get(71).str.split(',').str.get(0)
		except: result['CADD_rankscore'] = result['INFO'].str.split('|').str.get(71)
		# try: result['DANN_rankscore'] = result['INFO'].str.split('|').str.get(72).str.split(',').str.get(0)
		# except: result['DANN_rankscore'] = result['INFO'].str.split('|').str.get(72)
		# try: result['EigenPC_rankscore'] = result['INFO'].str.split('|').str.get(73).str.split(',').str.get(0)
		# except: result['EigenPC_rankscore'] = result['INFO'].str.split('|').str.get(73)
		# try: result['FATHMM_rankscore'] = result['INFO'].str.split('|').str.get(74).str.split(',').str.get(0)
		# except: result['FATHMM_rankscore'] = result['INFO'].str.split('|').str.get(74)
		# result['FATHMM_pred'] = result['INFO'].str.split('|').str.get(75)
		# try: result['GERP_rankscore'] = result['INFO'].str.split('|').str.get(76).str.split(',').str.get(0)
		# except: result['GERP_rankscore'] = result['INFO'].str.split('|').str.get(76)
		# result['Interpro_domain'] = result['INFO'].str.split('|').str.get(77)
		# try: result['LRT_rankscore'] = result['INFO'].str.split('|').str.get(78).str.split(',').str.get(0)
		# except: result['LRT_rankscore'] = result['INFO'].str.split('|').str.get(78)
		# result['LRT_pred'] = result['INFO'].str.split('|').str.get(79)
		# result['MCAP_pred'] = result['INFO'].str.split('|').str.get(80)
		# result['MetaLR_pred'] = result['INFO'].str.split('|').str.get(81)
		# try: result['MetaLR_rankscore'] = result['INFO'].str.split('|').str.get(82).str.split(',').str.get(0)
		# except:result['MetaLR_rankscore'] = result['INFO'].str.split('|').str.get(82)
		# result['MetaSVM_pred'] = result['INFO'].str.split('|').str.get(83)
		# try: result['MetaSVM_rankscore'] = result['INFO'].str.split('|').str.get(84).str.split(',').str.get(0)
		# except:result['MetaSVM_rankscore'] = result['INFO'].str.split('|').str.get(84)
		# try: result['MutPred_rankscore'] = result['INFO'].str.split('|').str.get(85).str.split(',').str.get(0)
		# except:result['MutPred_rankscore'] = result['INFO'].str.split('|').str.get(85)
		# result['MutationAssessor_pred'] = result['INFO'].str.split('|').str.get(86)
		# try: result['MutationAssessor_rankscore'] = result['INFO'].str.split('|').str.get(87).str.split(',').str.get(0)
		# except: result['MutationAssessor_rankscore'] = result['INFO'].str.split('|').str.get(87)
		# try: result['MutationTaster_rankscore'] = result['INFO'].str.split('|').str.get(88).str.split(',').str.get(0)
		# except:result['MutationTaster_rankscore'] = result['INFO'].str.split('|').str.get(88)
		# result['MutationTaster_pred'] = result['INFO'].str.split('|').str.get(89)
		# try: result['PROVEAN_rankscore'] = result['INFO'].str.split('|').str.get(90).str.split(',').str.get(0)
		# except:result['PROVEAN_rankscore'] = result['INFO'].str.split('|').str.get(90)
		# result['PROVEAN_pred'] = result['INFO'].str.split('|').str.get(91)
		# result['Polyphen2HDIV_pred'] = result['INFO'].str.split('|').str.get(92)
		# try: result['Polyphen2HDIV_rankscore'] = result['INFO'].str.split('|').str.get(93).str.split(',').str.get(0)
		# except:result['Polyphen2HDIV_rankscore'] = result['INFO'].str.split('|').str.get(93)
		# result['Polyphen2HVAR_pred'] = result['INFO'].str.split('|').str.get(94)
		# try: result['Polyphen2HVAR_rankscore'] = result['INFO'].str.split('|').str.get(95).str.split(',').str.get(0)
		# except:result['Polyphen2HVAR_rankscore'] = result['INFO'].str.split('|').str.get(95)
		# try: result['REVEL_rankscore'] = result['INFO'].str.split('|').str.get(96).str.split(',').str.get(0)
		# except:result['REVEL_rankscore'] = result['INFO'].str.split('|').str.get(96)
		# try: result['SIFT_rankscore'] = result['INFO'].str.split('|').str.get(97).str.split(',').str.get(0)
		# except:result['SIFT_rankscore'] = result['INFO'].str.split('|').str.get(97)
		# result['SIFT_pred'] = result['INFO'].str.split('|').str.get(98)
		# try: result['SiPhy29way_rankscore'] = result['INFO'].str.split('|').str.get(99).str.split(',').str.get(0)
		# except:result['SiPhy29way_rankscore'] = result['INFO'].str.split('|').str.get(99)
        #
		# try: result['VEST3_rankscore'] = result['INFO'].str.split('|').str.get(100).str.split(',').str.get(0)
		# except:result['VEST3_rankscore'] = result['INFO'].str.split('|').str.get(100)
        #
		# try: result['clinvar_MedGen_id'] = result['INFO'].str.split('|').str.get(101).str.split(',').str.get(0)
		# except:result['clinvar_MedGen_id'] = result['INFO'].str.split('|').str.get(101)
		# try: result['clinvar_OMIM_id'] = result['INFO'].str.split('|').str.get(102).str.split(',').str.get(0)
		# except:result['clinvar_OMIM_id'] = result['INFO'].str.split('|').str.get(102)
		# try: result['clinvar_Orphanet_id'] = result['INFO'].str.split('|').str.get(103).str.split(',').str.get(0)
		# except:result['clinvar_Orphanet_id'] = result['INFO'].str.split('|').str.get(103)
		# try: result['clinvar_clnsig'] = result['INFO'].str.split('|').str.get(104).str.split(',').str.get(0)
		# except:result['clinvar_clnsig'] = result['INFO'].str.split('|').str.get(104)
		# try: result['clinvar_review'] = result['INFO'].str.split('|').str.get(105).str.split(',').str.get(0)
		# except:result['clinvar_review'] = result['INFO'].str.split('|').str.get(105)
        #
		# try: result['fathmmMKL_pred'] = result['INFO'].str.split('|').str.get(106).str.split(',').str.get(0)
		# except: result['fathmmMKL_pred'] = result['INFO'].str.split('|').str.get(106)
        #
		# try: result['gnomAD_exomes_POPMAX_AF'] = result['INFO'].str.split('|').str.get(107).str.split(',').str.get(0)
		# except:result['gnomAD_exomes_POPMAX_AF'] = result['INFO'].str.split('|').str.get(107)
		# try: result['gnomAD_exomes_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(108).str.split(',').str.get(0)
		# except:result['gnomAD_exomes_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(108)
		# try: result['gnomAD_exomes_controls_AC'] = result['INFO'].str.split('|').str.get(109).str.split(',').str.get(0)
		# except:result['gnomAD_exomes_controls_AC'] = result['INFO'].str.split('|').str.get(109)
		# try: result['gnomAD_exomes_controls_AF'] = result['INFO'].str.split('|').str.get(110).str.split(',').str.get(0)
		# except:result['gnomAD_exomes_controls_AF'] = result['INFO'].str.split('|').str.get(110)
		# try: result['gnomAD_exomes_controls_nhomalt'] = result['INFO'].str.split('|').str.get(111).str.split(',').str.get(0)
		# except:result['gnomAD_exomes_controls_nhomalt'] = result['INFO'].str.split('|').str.get(111)
		# try: result['gnomAD_genomes_POPMAX_AF'] = result['INFO'].str.split('|').str.get(112).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_POPMAX_AF'] = result['INFO'].str.split('|').str.get(112)
		# try: result['gnomAD_genomes_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(113).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(113)
		# try: result['gnomAD_genomes_controls_AC'] = result['INFO'].str.split('|').str.get(114).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_controls_AC'] = result['INFO'].str.split('|').str.get(114)
		# try: result['gnomAD_genomes_controls_POPMAX_AF'] = result['INFO'].str.split('|').str.get(115).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_controls_POPMAX_AF'] = result['INFO'].str.split('|').str.get(115)
        #
		# try: result['gnomAD_genomes_controls_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(116).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_controls_POPMAX_nhomalt'] = result['INFO'].str.split('|').str.get(116)
		# try: result['gnomAD_genomes_controls_nhomalt'] = result['INFO'].str.split('|').str.get(117).str.split(',').str.get(0)
		# except:result['gnomAD_genomes_controls_nhomalt'] = result['INFO'].str.split('|').str.get(117)
		# try: result['phastCons100way_rankscore'] = result['INFO'].str.split('|').str.get(118).str.split(',').str.get(0)
		# except:result['phastCons100way_rankscore'] = result['INFO'].str.split('|').str.get(118)
		# try: result['phastCons20way_rankscore'] = result['INFO'].str.split('|').str.get(119).str.split(',').str.get(0)
		# except:result['phastCons20way_rankscore'] = result['INFO'].str.split('|').str.get(119)
		# try: result['phyloP100way_rankscore'] = result['INFO'].str.split('|').str.get(120).str.split(',').str.get(0)
		# except:result['phyloP100way_rankscore'] = result['INFO'].str.split('|').str.get(120)
		# try: result['phyloP20way_rankscore'] = result['INFO'].str.split('|').str.get(121).str.split(',').str.get(0)
		# except:result['phyloP20way_rankscore'] = result['INFO'].str.split('|').str.get(121)
		# try: result['ada_score'] = result['INFO'].str.split('|').str.get(122).str.split(',').str.get(0)
		# except:result['ada_score'] = result['INFO'].str.split('|').str.get(122)
		# try: result['rf_score'] = result['INFO'].str.split('|').str.get(123).str.split(',').str.get(0)
		# except:result['rf_score'] = result['INFO'].str.split('|').str.get(123)

		# mask1a = result['samtools'] == '0/1'
		# mask1a2 = result['samtools'] =='1/2'
		# mask1b = result['gatk'] == '0/1'
		# mask2a = result['samtools'] == '0/0'
		# mask2b = result['gatk'] == '0/0'
		# mask3a = result['samtools'] == '1/1'
		# mask3b = result['gatk'] == '1/1'
		# result.loc[mask1a, 'samtools_geno'] = 'het'
		# result.loc[mask1a2, 'samtools_geno'] = 'het'
		# result.loc[mask1b, 'gatk_geno'] = 'het'
		# result.loc[mask2a, 'samtools_geno'] = 'homo_wild'
		# result.loc[mask2b, 'gatk_geno'] = 'homo_wild'
		# result.loc[mask3a, 'samtools_geno'] = 'homo'
		# result.loc[mask3b, 'gatk_geno'] = 'homo'
		mask1a = result['geno_phasato'] == '0|1'
		mask1anp = result['geno_phasato'] == '0/1'
		mask1a2 = result['geno_phasato'] =='1|2'
		mask1a2np = result['geno_phasato'] == '1/2'
		mask1b = result['geno_phasato'] =='1|0'
		mask1bnp = result['geno_phasato'] == '1/0'

		#mask1b = result['gatk'] == '0/1'
		mask2a = result['geno_phasato'] == '0|0'
		mask2anp = result['geno_phasato'] == '0/0'
		mask3anp = result['geno_phasato'] == '1/1'
		#mask2b = result['gatk'] == '0/0'
		mask3a = result['geno_phasato'] == '1|1'
		#mask3b = result['gatk'] == '1/1'
		result.loc[mask1a, 'geno_descripion'] = 'het'
		result.loc[mask1anp, 'geno_descripion'] = 'het_not_ph'
		result.loc[mask1a2, 'geno_descripion'] = 'het'
		result.loc[mask1a2np, 'geno_descripion'] = 'het_not_ph'
		result.loc[mask1b, 'geno_descripion'] = 'het'
		result.loc[mask1bnp, 'geno_descripion'] = 'het_not_ph'
		#result.loc[mask1b, 'gatk_geno'] = 'het'
		result.loc[mask2a, 'geno_descripion'] = 'homo_wild'
		result.loc[mask2anp, 'geno_descripion'] = 'homo_wild_not_ph'
		#result.loc[mask2b, 'gatk_geno'] = 'homo_wild'
		result.loc[mask3a, 'geno_descripion'] = 'homo'
		result.loc[mask3anp, 'geno_descripion'] = 'homo_not_ph'
		#result.loc[mask3b, 'gatk_geno'] = 'homo'
		#print(result[['geno_phasato','geno_descripion']])

		# HGMD['POS'] = HGMD['END']
		# result2 = pd.merge(result,HGMD,on=['#CHROM','POS'],how='left')
		# result2.drop_duplicates(subset=['#CHROM','POS'],inplace=True)
		# result2['hgmd'] = result2['hgmd_x']
		# #print result2[['hgmd_x','hgmd_y']]
		# try:
		# 	result2['hgmd_mutation'] = result2['hgmd_y'].str.split('|').str.get(2)
		# 	result2['hgmd_function'] = result2['hgmd_y'].str.split('|').str.get(3)
		# 	result2['hgmd_phenotype'] = result2['hgmd_y'].str.split('|').str.get(4)
		# 	result2['hgmd_pubmed'] = result2['hgmd_y'].str.split('|').str.get(5)
		# except AttributeError:
		# 	result2['hgmd_mutation'] = ''
		# 	result2['hgmd_function'] = ''
		# 	result2['hgmd_phenotype'] = ''
		# 	result2['hgmd_pubmed'] = ''
		# 	print ('AttributeError!!!')
        #
		# result2.drop('hgmd_y',axis=1,inplace=True)
		# result2.drop('hgmd_x',axis=1,inplace=True)
		# result2['length'] = result2['length'].map('{:,.0f}'.format)
        #
		# for index, row in result2.iterrows():
		# 	count_ref = len(row['REF'])
		# 	count_alt = len(row['ALT'])
		# 	result2.loc[index,'count_ref'] = int(count_ref)
		# 	result2.loc[index,'count_alt'] = int(count_alt)
		# result2['START'] = np.where((result2['count_ref']>1),
		# 			result2['POS']+1,result2['POS'])
        #
		# result2['END'] = np.where((result2['count_ref'] > result2['count_alt']),
		# 			result2['POS']+(result2['count_ref']-1),
		# 			np.where((result2['count_ref'] < result2['count_alt']),
		# 			result2['START']+((result2['count_alt']-1)-(result2['count_ref']-1)),
		# 			result2['POS']))
        #
		# result2['count_ref'] = result2['count_ref'].astype(int)
		# result2['count_alt'] = result2['count_alt'].astype(int)
		# for index, row in result2.iterrows():
		# 	if (row['count_ref'] > row['count_alt'])&(row['count_ref']>1):
		# 		row['REF_2'] = row['REF'][row['count_alt']:]
		# 		row['ALT_2'] = '-'
		# 		result2.loc[index,'REF_2'] = row['REF_2']
		# 		result2.loc[index,'ALT_2'] = row['ALT_2']
		# 		result2.loc[index,'types'] = 'DELETION'
		# 	elif (row['count_ref'] < row['count_alt'])&(row['count_alt']>1):
		# 		row['REF_2'] = '-'
		# 		row['ALT_2'] = row['ALT'][row['count_ref']:]
		# 		result2.loc[index,'REF_2'] = row['REF_2']
		# 		result2.loc[index,'ALT_2'] = row['ALT_2']
		# 		result2.loc[index,'types'] = 'INSERTION'
		# 	elif (row['count_ref'] == row['count_alt'])&(row['count_ref'] == 1):
		# 		row['REF_2'] = row['REF']
		# 		row['ALT_2'] = row['ALT']
		# 		result2.loc[index,'REF_2'] = row['REF_2']
		# 		result2.loc[index,'ALT_2'] = row['ALT_2']
		# 		result2.loc[index,'types'] = 'SVN'
        #
		# result2['START'] = result2['START'].astype(int)
		# result2['END'] = result2['END'].astype(int)
		# result2['HGVS'] = result2['#CHROM']+':'+result2['START'].astype(str)+'-'+result2['END'].astype(str)+':'+result2['REF_2']+'/'+result2['ALT_2']
        #
		# result2['HGVS_c'].fillna('',inplace=True)
		# result2['HGVS_p'].fillna('',inplace=True)
        #
        #
		# for index, row in result2.iterrows():
		# 	if row['strand'] == str(-1):
		# 		#print row['REF_2'],row['REF']
		# 		try: seq_x = Seq(row['REF_2'], IUPAC.unambiguous_dna)
		# 		except: seq_x = Seq(row['REF'], IUPAC.unambiguous_dna)
		# 		try: seq_y = Seq(row['ALT_2'], IUPAC.unambiguous_dna)
		# 		except: seq_x = Seq(row['ALT'], IUPAC.unambiguous_dna)
		# 		seq_ref = seq_x.reverse_complement()
		# 		seq_alt = seq_y.reverse_complement()
		# 		row['REF_2'] = seq_ref
		# 		row['ALT_2'] = seq_alt
		# 	#print row['HGVS_c']
		# 	#try:
		# 	row['HGVS_c'] = row['HGVS_c'].replace('N>',str(row['REF_2']+'>'))
		# 	DEL = 'del'+'N'*(len(row['REF_2']))
		# 	row['HGVS_c'] = row['HGVS_c'].replace(DEL,'del'+str(row['REF_2']))
		# 	#except:  #row['HGVS_c'] = ''
		# 	result2.loc[index,'HGVS_c'] = row['HGVS_c']
        #
        #
		# mask1 = result2['HGVS_p'].str.contains('p.%3D')
		# result2.loc[mask1,'HGVS_p'] = '(p.%3D)'
        #
		# result2['HGVS_c'] = result2['HGVS_c'].replace(r'NN*','',regex=True)
		# result2['HGVS_c'] = result2['HGVS_c'].replace(r'delN+','',regex=True)
		# result2['HGVS_c'] = result2['HGVS_c'].replace(r'del-','',regex=True)
		# result2['HGVS_c'] = result2['HGVS_c'].replace(r'^M_','NM_',regex=True)
		# result2['HGVS_c'] = result2['HGVS_c'].replace(r'^R_','NR_',regex=True)
        #
		# result2['refseq'].fillna(result2['HGVS_c'].str.split('.').str.get(0),inplace=True)
		# result2['variation_cosmic'].fillna('RISN',inplace=True)
        #
		# mask1 = result2['variation_cosmic'].str.contains('RISN')
		# result2.loc[mask1,'variation_cosmic'] = ''
		# mask2 = result2['variation_cosmic'].str.contains('rs')
		# result2.loc[mask2,'variation_cosmic'] = ''
		# mask3 = result2['variation_cosmic'].str.contains('RPGR')
		# result2.loc[mask3,'variation_cosmic'] = ''
        #
		# result2['hgmd_mutation'].replace('null;null','',inplace=True)
		# result2['CHROM'] = result2['#CHROM']
		# result2['ALTGENE'] = result2['GENE']
		# result2['GENE'] = result2['symbol']
        #
		# #result2['GENE'] = result2['GENE'].fillna(result2['symbol'])
		# #result2['GENE'] = result2['symbol']
        #
		# result2['sample'] = str(name)
		# #result2b = result2[result2['QUAL'] >= 18]
        #
		# result2b = result2
		# result2b['hgmd_mutation'].fillna('',inplace=True)
		# result2b.sort_values(by=['#CHROM','POS'],ascending=[True,True],inplace=True)
		# result2b['hgmd_pubmed'] = result2b['hgmd_pubmed'].str.decode('iso-8859-1').str.encode('utf-8')
		# result2b['hgmd_pubmed'] = result2b['hgmd_pubmed'].str.decode('cp1252').str.encode('utf-8')
		# result2b['hgmd_pubmed'] = result2b['hgmd_pubmed'].str.decode('iso-8859-15').str.encode('utf-8')
		# result2b['hgmd_pubmed'] = result2b['hgmd_pubmed'].str.decode('latin-1').str.encode('utf-8')
		# result2b['hgmd_phenotype'] = result2b['hgmd_phenotype'].str.decode('iso-8859-1').str.encode('utf-8')
		# result2b['hgmd_phenotype'] = result2b['hgmd_phenotype'].str.decode('cp1252').str.encode('utf-8')
		# result2b['hgmd_phenotype'] = result2b['hgmd_phenotype'].str.decode('iso-8859-15').str.encode('utf-8')
		# result2b['hgmd_phenotype'] = result2b['hgmd_phenotype'].str.decode('latin-1').str.encode('utf-8')
		# result2b['hgmd_function'] = result2b['hgmd_function'].str.decode('iso-8859-1').str.encode('utf-8')
		# result2b['hgmd_function'] = result2b['hgmd_function'].str.decode('cp1252').str.encode('utf-8')
		# result2b['hgmd_function'] = result2b['hgmd_function'].str.decode('iso-8859-15').str.encode('utf-8')
		# result2b['hgmd_function'] = result2b['hgmd_function'].str.decode('latin-1').str.encode('utf-8')
		# result2b['hgmd_mutation'] = result2b['hgmd_mutation'].str.decode('iso-8859-1').str.encode('utf-8')
		# result2b['hgmd_mutation'] = result2b['hgmd_mutation'].str.decode('cp1252').str.encode('utf-8')
		# result2b['hgmd_mutation'] = result2b['hgmd_mutation'].str.decode('iso-8859-15').str.encode('utf-8')
		# result2b['hgmd_mutation'] = result2b['hgmd_mutation'].str.decode('latin-1').str.encode('utf-8')
        #
		# result2b['DEPTH'] = result2b['DEPTH'].str.split(',').str.get(0)
		# result2b['DEPTH2'].fillna(result2b['DEPTH'],inplace=True)
		# result2b['DEPTH'] = result2b['DEPTH2'].astype(int)
        #
		# result2b['mapquality'].fillna(-999,inplace=True)
		# result2b['mapquality2'] = result2b['mapquality'].astype(float)
		# result2b['mapquality'] = result2b['mapquality2'].astype(int)
		# result2b['GENE'].replace('DFNB31','WHRN',inplace=True)
		##result3 = result2b[cols]
		result3 = result
		#print result3[result3['POS']==34449523]

		final = ''.join([folder,name+'.longshot.filt.appris.vcf'])
		result3[cols].to_csv(final,sep='\t',index=False,encoding='utf-8')
		print('Annotation CDS len:', len(result3),'->',str(name))

	else:
		result3 = pd.DataFrame(columns=cols)
		final = ''.join([folder,name+'.longshot.filt.appris.vcf'])
		result3.to_csv(final,sep='\t',index=False,encoding='utf-8')
		print ('Annotation CDS len:', len(result3),'->',str(name))

	return result3

if __name__=="__main__":
    args = InputPar()
    folder_name = step.principal_folder(args,step.create_folder(),over='False')
    # sample = 'RE1644.2020'
    # project = '19_Jan_2021_RPE65_2'
    # path = '/home/remo/PROJECT/pacbio/'
    # path_res = path + 'RESULT/'
    print('--------------------------------------------------------------------')
    print(folder_name)
    files = glob.glob(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))

    print(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))
    print(files)
    try:
        os.mkdir(folder_name+'/06_1-appris_filtering/')
    except:
        pass

    for folder_sample in files:

        sample_x = folder_sample.split('/')[-1].split('.longshot.filt.vcf')[0]
        sample = str(sample_x)
        print ('----->'+sample+'<------')
        print('cp ' + folder_sample + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        #+ path_res +project+'/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        os.system('cp ' + folder_sample + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        vcf_l = read_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        #print(vcf_l)
        APPRIS = pd.read_csv(appris_principal, sep = '\t')
        result3 = final_annotation(vcf_l, APPRIS, folder_name + '/06_1-appris_filtering/', sample)
        #print(result3.columns.to_csv('columns_info.csv'))
        # pd.Series(result3.columns,index=result3.columns).to_csv('columns_info.csv', header=False)
        #
        print(result3.dropna(axis = 1, how='all').columns)
